<?php
class Home extends MY_Controller {

  function index() {
    $data['title'] = 'Beranda';
    $this->load->model('mpost');

    $this->db->select('
    bmkg_tforecast.*,
    w1.Nm_Icon_Day as Ic_FC1,
    w2.Nm_Icon_Day as Ic_FC2,
    w3.Nm_Icon_Day as Ic_FC3,
    w4.Nm_Icon_Day as Ic_FC4,
    w1.Nm_Weather as Nm_FC1,
    w2.Nm_Weather as Nm_FC2,
    w3.Nm_Weather as Nm_FC3,
    w4.Nm_Weather as Nm_FC4,
    wt1.Nm_Icon_Day as Ic_FCT1,
    wt2.Nm_Icon_Day as Ic_FCT2,
    wt3.Nm_Icon_Day as Ic_FCT3,
    wt4.Nm_Icon_Day as Ic_FCT4,
    wt1.Nm_Weather as Nm_FCT1,
    wt2.Nm_Weather as Nm_FCT2,
    wt3.Nm_Weather as Nm_FCT3,
    wt4.Nm_Weather as Nm_FCT4');
    $this->db->join(TBL_BMKG_MWEATHER.' w1', 'w1.Kd_Weather = bmkg_tforecast.FC1_Kd_Weather', 'left');
    $this->db->join(TBL_BMKG_MWEATHER.' w2', 'w2.Kd_Weather = bmkg_tforecast.FC2_Kd_Weather', 'left');
    $this->db->join(TBL_BMKG_MWEATHER.' w3', 'w3.Kd_Weather = bmkg_tforecast.FC3_Kd_Weather', 'left');
    $this->db->join(TBL_BMKG_MWEATHER.' w4', 'w4.Kd_Weather = bmkg_tforecast.FC4_Kd_Weather', 'left');
    $this->db->join(TBL_BMKG_MWEATHER.' wt1', 'wt1.Kd_Weather = bmkg_tforecast.FCT1_Kd_Weather', 'left');
    $this->db->join(TBL_BMKG_MWEATHER.' wt2', 'wt2.Kd_Weather = bmkg_tforecast.FCT2_Kd_Weather', 'left');
    $this->db->join(TBL_BMKG_MWEATHER.' wt3', 'wt3.Kd_Weather = bmkg_tforecast.FCT3_Kd_Weather', 'left');
    $this->db->join(TBL_BMKG_MWEATHER.' wt4', 'wt4.Kd_Weather = bmkg_tforecast.FCT4_Kd_Weather', 'left');

    $this->db->where(COL_TANGGAL, date('Y-m-d'));
    $this->db->where(COL_PUBLISHED, 1);
    $data['res'] = $this->db->get(TBL_BMKG_TFORECAST)->row_array();
    $data['berita'] = $this->mpost->search(5,"",1);

		$this->template->set('title', 'Home');
		$this->template->load('frontend' , 'home/index', $data);
    //$this->load->view('home/index');
  }

  function _404() {
    $data['title'] = 'Error';
    if(IsLogin()) {
      $this->template->load('backend' , 'home/_error', $data);
    } else {
      $this->template->load('frontend' , 'home/_error', $data);
    }
  }

  public function page($slug) {
    $data['title'] = 'Page';

    $this->db->join(TBL__POSTCATEGORIES,TBL__POSTCATEGORIES.'.'.COL_POSTCATEGORYID." = ".TBL__POSTS.".".COL_POSTCATEGORYID,"inner");
    $this->db->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL__POSTS.".".COL_CREATEDBY,"inner");
    $this->db->where("(".TBL__POSTS.".".COL_POSTSLUG." = '".$slug."' OR ".TBL__POSTS.".".COL_POSTID." = '".$slug."')");
    $rpost = $this->db->get(TBL__POSTS)->row_array();
    if(!$rpost) {
        show_404();
        return false;
    }

    $this->db->where(COL_POSTID, $rpost[COL_POSTID]);
    $this->db->set(COL_TOTALVIEW, COL_TOTALVIEW."+1", FALSE);
    $this->db->update(TBL__POSTS);

    $data['title'] = $rpost[COL_POSTCATEGORYNAME];//strlen($rpost[COL_POSTTITLE]) > 50 ? substr($rpost[COL_POSTTITLE], 0, 50) . "..." : $rpost[COL_POSTTITLE];
    $data['data'] = $rpost;
    $this->template->load('frontend' , 'home/page', $data);
  }

  public function post($cat) {
    $data['title'] = 'Tautan';

    $this->db->join(TBL__POSTCATEGORIES,TBL__POSTCATEGORIES.'.'.COL_POSTCATEGORYID." = ".TBL__POSTS.".".COL_POSTCATEGORYID,"inner");
    $this->db->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL__POSTS.".".COL_CREATEDBY,"inner");
    $this->db->where(TBL__POSTS.".".COL_POSTCATEGORYID, $cat);
    $this->db->order_by(TBL__POSTS.".".COL_CREATEDON, 'desc');
    $data['res'] = $rpost = $this->db->get(TBL__POSTS)->result_array();

    if(!empty($rpost)) {
      $data['title'] = $rpost[0][COL_POSTCATEGORYNAME];
    }

    $data['data'] = $rpost;
    $this->template->load('frontend' , 'home/post', $data);
  }
}
 ?>
