<?php
class Data extends MY_Controller {
  public function __construct()
  {
      parent::__construct();
      if (!IsLogin()) {
          redirect('site/user/login');
      }
  }

  public function forecast_index() {
    $data['title'] = "Prakiraan Cuaca";
    $data['data'] = '';
    $this->template->load('backend', 'data/index_forecast', $data);
  }

  public function forecast_index_load() {
    $orderdef = array(COL_TANGGAL=>'desc');
    $orderables = array(null,COL_TANGGAL, COL_PUBLISHED, COL_CREATE_BY, COL_CREATE_BY);
    $cols = array(COL_TANGGAL, COL_PUBLISHED, COL_CREATE_BY, COL_CREATE_BY);

    $i = 0;
    foreach($cols as $item){
      if($_POST['search']['value']){
        if($i===0){
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        }else{
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(isset($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(isset($orderdef)){
        $order = $orderdef;
        $this->db->order_by(key($order), $order[key($order)]);
    }

    $this->db->select('
    bmkg_tforecast.*,
    w1.Nm_Icon_Day as Ic_FC1,
    w2.Nm_Icon_Day as Ic_FC2,
    w3.Nm_Icon_Day as Ic_FC3,
    w4.Nm_Icon_Day as Ic_FC4,
    w1.Nm_Weather as Nm_FC1,
    w2.Nm_Weather as Nm_FC2,
    w3.Nm_Weather as Nm_FC3,
    w4.Nm_Weather as Nm_FC4,
    wt1.Nm_Icon_Day as Ic_FCT1,
    wt2.Nm_Icon_Day as Ic_FCT2,
    wt3.Nm_Icon_Day as Ic_FCT3,
    wt4.Nm_Icon_Day as Ic_FCT4,
    wt1.Nm_Weather as Nm_FCT1,
    wt2.Nm_Weather as Nm_FCT2,
    wt3.Nm_Weather as Nm_FCT3,
    wt4.Nm_Weather as Nm_FCT4');
    $this->db->join(TBL_BMKG_MWEATHER.' w1', 'w1.Kd_Weather = bmkg_tforecast.FC1_Kd_Weather', 'left');
    $this->db->join(TBL_BMKG_MWEATHER.' w2', 'w2.Kd_Weather = bmkg_tforecast.FC2_Kd_Weather', 'left');
    $this->db->join(TBL_BMKG_MWEATHER.' w3', 'w3.Kd_Weather = bmkg_tforecast.FC3_Kd_Weather', 'left');
    $this->db->join(TBL_BMKG_MWEATHER.' w4', 'w4.Kd_Weather = bmkg_tforecast.FC4_Kd_Weather', 'left');
    $this->db->join(TBL_BMKG_MWEATHER.' wt1', 'wt1.Kd_Weather = bmkg_tforecast.FCT1_Kd_Weather', 'left');
    $this->db->join(TBL_BMKG_MWEATHER.' wt2', 'wt2.Kd_Weather = bmkg_tforecast.FCT2_Kd_Weather', 'left');
    $this->db->join(TBL_BMKG_MWEATHER.' wt3', 'wt3.Kd_Weather = bmkg_tforecast.FCT3_Kd_Weather', 'left');
    $this->db->join(TBL_BMKG_MWEATHER.' wt4', 'wt4.Kd_Weather = bmkg_tforecast.FCT4_Kd_Weather', 'left');
    $query = $this->db->get(TBL_BMKG_TFORECAST);
    $data = [];

    foreach($query->result_array() as $r) {
      $data[] = array(
        '<input type="checkbox" class="cekbox" name="cekbox[]" value="' . $r[COL_UNIQ] . '" />',
        anchor('site/data/forecast-edit/'.$r[COL_UNIQ], date('Y-m-d', strtotime($r[COL_TANGGAL]))),
        '<i class="fad fa-lg fa-'.(!empty($r['Ic_FC1'])?$r['Ic_FC1']:'question-circle').'" title="Pagi: '.(!empty($r['Nm_FC1'])?$r['Nm_FC1']:'').'"></i>&nbsp'.
        '<i class="fad fa-lg fa-'.(!empty($r['Ic_FC2'])?$r['Ic_FC2']:'question-circle').'" title="Siang: '.(!empty($r['Nm_FC2'])?$r['Nm_FC2']:'').'"></i>&nbsp'.
        '<i class="fad fa-lg fa-'.(!empty($r['Ic_FC3'])?$r['Ic_FC3']:'question-circle').'" title="Sore: '.(!empty($r['Nm_FC3'])?$r['Nm_FC3']:'').'"></i>&nbsp'.
        '<i class="fad fa-lg fa-'.(!empty($r['Ic_FC4'])?$r['Ic_FC4']:'question-circle').'" title="Malam: '.(!empty($r['Nm_FC4'])?$r['Nm_FC4']:'').'"></i>',
        '<i class="fad fa-lg fa-'.(!empty($r['Ic_FCT1'])?$r['Ic_FCT1']:'question-circle').'" title="Pagi: '.(!empty($r['Nm_FCT1'])?$r['Nm_FCT1']:'').'"></i>&nbsp'.
        '<i class="fad fa-lg fa-'.(!empty($r['Ic_FCT2'])?$r['Ic_FCT2']:'question-circle').'" title="Siang: '.(!empty($r['Nm_FCT2'])?$r['Nm_FCT2']:'').'"></i>&nbsp'.
        '<i class="fad fa-lg fa-'.(!empty($r['Ic_FCT3'])?$r['Ic_FCT3']:'question-circle').'" title="Sore: '.(!empty($r['Nm_FCT3'])?$r['Nm_FCT3']:'').'"></i>&nbsp'.
        '<i class="fad fa-lg fa-'.(!empty($r['Ic_FCT4'])?$r['Ic_FCT4']:'question-circle').'" title="Malam: '.(!empty($r['Nm_FCT4'])?$r['Nm_FCT4']:'').'"></i>',
        $r[COL_PUBLISHED]?'<i class="fad fa-lg fa-check-square text-success"></i>':'<i class="fad fa-lg fa-times-square text-danger"></i>',
        $r[COL_CREATE_BY],
        date('Y-m-d', strtotime($r[COL_CREATE_DATE])),
      );
    }


    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $query->num_rows(),
      "recordsTotal" => $this->db->count_all_results(TBL_BMKG_TFORECAST),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function forecast_add() {
    $ruser = GetLoggedUser();
    $data['title'] = 'Prakiraan Cuaca';
    $data['edit'] = false;
    if (!empty($_POST)) {
      $data['data'] = $_POST;
      $alertToday = $this->input->post("FC_Alert");
      $alertTomorrow = $this->input->post("FCT_Alert");
      if(!empty($alertToday)) $alertToday = explode(' - ', $alertToday);
      if(!empty($alertTomorrow)) $alertTomorrow = explode(' - ', $alertTomorrow);

      $config['upload_path'] = MY_UPLOADPATH;
      $config['allowed_types'] = "gif|jpg|jpeg|png";
      $config['max_size']	= 512000;
      $config['max_width']  = 4000;
      $config['max_height']  = 4000;
      $config['overwrite'] = FALSE;
      $this->load->library('upload',$config);

      $fileFC = count($_FILES['file_fc1']['name']);
      $uploadFC = array();
      if($fileFC > 0 && !empty($_FILES['file_fc1']['name'][0])) {
        for($i = 0; $i < $fileFC; $i++) {
          $_FILES['file']['name'] = $_FILES['file_fc1']['name'][$i];
          $_FILES['file']['type'] = $_FILES['file_fc1']['type'][$i];
          $_FILES['file']['tmp_name'] = $_FILES['file_fc1']['tmp_name'][$i];
          $_FILES['file']['error'] = $_FILES['file_fc1']['error'][$i];
          $_FILES['file']['size'] = $_FILES['file_fc1']['size'][$i];

          if($this->upload->do_upload('file')){
            $fileData = $this->upload->data();
            $uploadFC[] = $fileData['file_name'];
          }
          else {
            $data['upload_errors'] = $this->upload->display_errors();
            $this->template->load('backend', 'data/form_forecast', $data);
            return;
          }
        }
        $rec[COL_FC1_IMAGES] = !empty($uploadFC)?json_encode($uploadFC):null;
      }

      $fileFC = count($_FILES['file_fc2']['name']);
      $uploadFC = array();
      if($fileFC > 0 && !empty($_FILES['file_fc2']['name'][0])) {
        for($i = 0; $i < $fileFC; $i++) {
          $_FILES['file']['name'] = $_FILES['file_fc2']['name'][$i];
          $_FILES['file']['type'] = $_FILES['file_fc2']['type'][$i];
          $_FILES['file']['tmp_name'] = $_FILES['file_fc2']['tmp_name'][$i];
          $_FILES['file']['error'] = $_FILES['file_fc2']['error'][$i];
          $_FILES['file']['size'] = $_FILES['file_fc2']['size'][$i];

          if($this->upload->do_upload('file')){
            $fileData = $this->upload->data();
            $uploadFC[] = $fileData['file_name'];
          }
          else {
            $data['upload_errors'] = $this->upload->display_errors();
            $this->template->load('backend', 'data/form_forecast', $data);
            return;
          }
        }
        $rec[COL_FC2_IMAGES] = !empty($uploadFC)?json_encode($uploadFC):null;
      }

      $fileFC = count($_FILES['file_fc3']['name']);
      $uploadFC = array();
      if($fileFC > 0 && !empty($_FILES['file_fc3']['name'][0])) {
        for($i = 0; $i < $fileFC; $i++) {
          $_FILES['file']['name'] = $_FILES['file_fc3']['name'][$i];
          $_FILES['file']['type'] = $_FILES['file_fc3']['type'][$i];
          $_FILES['file']['tmp_name'] = $_FILES['file_fc3']['tmp_name'][$i];
          $_FILES['file']['error'] = $_FILES['file_fc3']['error'][$i];
          $_FILES['file']['size'] = $_FILES['file_fc3']['size'][$i];

          if($this->upload->do_upload('file')){
            $fileData = $this->upload->data();
            $uploadFC[] = $fileData['file_name'];
          }
          else {
            $data['upload_errors'] = $this->upload->display_errors();
            $this->template->load('backend', 'data/form_forecast', $data);
            return;
          }
        }
        $rec[COL_FC3_IMAGES] = !empty($uploadFC)?json_encode($uploadFC):null;
      }

      $fileFC = count($_FILES['file_fc4']['name']);
      $uploadFC = array();
      if($fileFC > 0 && !empty($_FILES['file_fc4']['name'][0])) {
        for($i = 0; $i < $fileFC; $i++) {
          $_FILES['file']['name'] = $_FILES['file_fc4']['name'][$i];
          $_FILES['file']['type'] = $_FILES['file_fc4']['type'][$i];
          $_FILES['file']['tmp_name'] = $_FILES['file_fc4']['tmp_name'][$i];
          $_FILES['file']['error'] = $_FILES['file_fc4']['error'][$i];
          $_FILES['file']['size'] = $_FILES['file_fc4']['size'][$i];

          if($this->upload->do_upload('file')){
            $fileData = $this->upload->data();
            $uploadFC[] = $fileData['file_name'];
          }
          else {
            $data['upload_errors'] = $this->upload->display_errors();
            $this->template->load('backend', 'data/form_forecast', $data);
            return;
          }
        }
        $rec[COL_FC4_IMAGES] = !empty($uploadFC)?json_encode($uploadFC):null;
      }

      $fileFC = count($_FILES['file_fct1']['name']);
      $uploadFC = array();
      if($fileFC > 0 && !empty($_FILES['file_fct1']['name'][0])) {
        for($i = 0; $i < $fileFC; $i++) {
          $_FILES['file']['name'] = $_FILES['file_fct1']['name'][$i];
          $_FILES['file']['type'] = $_FILES['file_fct1']['type'][$i];
          $_FILES['file']['tmp_name'] = $_FILES['file_fct1']['tmp_name'][$i];
          $_FILES['file']['error'] = $_FILES['file_fct1']['error'][$i];
          $_FILES['file']['size'] = $_FILES['file_fct1']['size'][$i];

          if($this->upload->do_upload('file')){
            $fileData = $this->upload->data();
            $uploadFC[] = $fileData['file_name'];
          }
          else {
            $data['upload_errors'] = $this->upload->display_errors();
            $this->template->load('backend', 'data/form_forecast', $data);
            return;
          }
        }
        $rec[COL_FCT1_IMAGES] = !empty($uploadFC)?json_encode($uploadFC):null;
      }

      $fileFC = count($_FILES['file_fct2']['name']);
      $uploadFC = array();
      if($fileFC > 0 && !empty($_FILES['file_fct2']['name'][0])) {
        for($i = 0; $i < $fileFC; $i++) {
          $_FILES['file']['name'] = $_FILES['file_fct2']['name'][$i];
          $_FILES['file']['type'] = $_FILES['file_fct2']['type'][$i];
          $_FILES['file']['tmp_name'] = $_FILES['file_fct2']['tmp_name'][$i];
          $_FILES['file']['error'] = $_FILES['file_fct2']['error'][$i];
          $_FILES['file']['size'] = $_FILES['file_fct2']['size'][$i];

          if($this->upload->do_upload('file')){
            $fileData = $this->upload->data();
            $uploadFC[] = $fileData['file_name'];
          }
          else {
            $data['upload_errors'] = $this->upload->display_errors();
            $this->template->load('backend', 'data/form_forecast', $data);
            return;
          }
        }
        $rec[COL_FCT2_IMAGES] = !empty($uploadFC)?json_encode($uploadFC):null;
      }

      $fileFC = count($_FILES['file_fct3']['name']);
      $uploadFC = array();
      if($fileFC > 0 && !empty($_FILES['file_fct3']['name'][0])) {
        for($i = 0; $i < $fileFC; $i++) {
          $_FILES['file']['name'] = $_FILES['file_fct3']['name'][$i];
          $_FILES['file']['type'] = $_FILES['file_fct3']['type'][$i];
          $_FILES['file']['tmp_name'] = $_FILES['file_fct3']['tmp_name'][$i];
          $_FILES['file']['error'] = $_FILES['file_fct3']['error'][$i];
          $_FILES['file']['size'] = $_FILES['file_fct3']['size'][$i];

          if($this->upload->do_upload('file')){
            $fileData = $this->upload->data();
            $uploadFC[] = $fileData['file_name'];
          }
          else {
            $data['upload_errors'] = $this->upload->display_errors();
            $this->template->load('backend', 'data/form_forecast', $data);
            return;
          }
        }
        $rec[COL_FCT3_IMAGES] = !empty($uploadFC)?json_encode($uploadFC):null;
      }

      $fileFC = count($_FILES['file_fct4']['name']);
      $uploadFC = array();
      if($fileFC > 0 && !empty($_FILES['file_fct4']['name'][0])) {
        for($i = 0; $i < $fileFC; $i++) {
          $_FILES['file']['name'] = $_FILES['file_fct4']['name'][$i];
          $_FILES['file']['type'] = $_FILES['file_fct4']['type'][$i];
          $_FILES['file']['tmp_name'] = $_FILES['file_fct4']['tmp_name'][$i];
          $_FILES['file']['error'] = $_FILES['file_fct4']['error'][$i];
          $_FILES['file']['size'] = $_FILES['file_fct4']['size'][$i];

          if($this->upload->do_upload('file')){
            $fileData = $this->upload->data();
            $uploadFC[] = $fileData['file_name'];
          }
          else {
            $data['upload_errors'] = $this->upload->display_errors();
            $this->template->load('backend', 'data/form_forecast', $data);
            return;
          }
        }
        $rec[COL_FCT4_IMAGES] = !empty($uploadFC)?json_encode($uploadFC):null;
      }

      $rec = array(
        COL_TANGGAL=>$this->input->post(COL_TANGGAL),
        COL_ALERT_FROM=>!empty($alertToday)?reset($alertToday):null,
        COL_ALERT_TO=>!empty($alertToday)?end($alertToday):null,
        COL_ALERT_TEXT=>$this->input->post(COL_ALERT_TEXT),
        COL_ALERT_WARNING=>$this->input->post(COL_ALERT_WARNING),
        COL_ALERT_TFROM=>!empty($alertTomorrow)?reset($alertTomorrow):null,
        COL_ALERT_TTO=>!empty($alertTomorrow)?end($alertTomorrow):null,
        COL_ALERT_TTEXT=>$this->input->post(COL_ALERT_TTEXT),
        COL_ALERT_TWARNING=>$this->input->post(COL_ALERT_TWARNING),

        COL_FC1_KD_WEATHER=>$this->input->post(COL_FC1_KD_WEATHER),
        COL_FC1_DESC=>$this->input->post(COL_FC1_DESC),
        COL_FC2_KD_WEATHER=>$this->input->post(COL_FC2_KD_WEATHER),
        COL_FC2_DESC=>$this->input->post(COL_FC2_DESC),
        COL_FC3_KD_WEATHER=>$this->input->post(COL_FC3_KD_WEATHER),
        COL_FC3_DESC=>$this->input->post(COL_FC3_DESC),
        COL_FC4_KD_WEATHER=>$this->input->post(COL_FC4_KD_WEATHER),
        COL_FC4_DESC=>$this->input->post(COL_FC4_DESC),
        COL_FCT1_KD_WEATHER=>$this->input->post(COL_FCT1_KD_WEATHER),
        COL_FCT1_DESC=>$this->input->post(COL_FCT1_DESC),
        COL_FCT2_KD_WEATHER=>$this->input->post(COL_FCT2_KD_WEATHER),
        COL_FCT2_DESC=>$this->input->post(COL_FCT2_DESC),
        COL_FCT3_KD_WEATHER=>$this->input->post(COL_FCT3_KD_WEATHER),
        COL_FCT3_DESC=>$this->input->post(COL_FCT3_DESC),
        COL_FCT4_KD_WEATHER=>$this->input->post(COL_FCT4_KD_WEATHER),
        COL_FCT4_DESC=>$this->input->post(COL_FCT4_DESC),


        COL_CREATE_BY=>$ruser[COL_USERNAME],
        COL_CREATE_DATE=>date('Y-m-d H:i:s')
      );

      $res = $this->db->insert(TBL_BMKG_TFORECAST, $rec);
      if ($res) {
          redirect('site/data/forecast-index');
      } else {
        $data['err'] = $this->db->error();
        $this->template->load('backend', 'data/form_forecast', $data);
      }

    } else {
      $this->template->load('backend', 'data/form_forecast', $data);
    }
  }

  public function forecast_edit($id) {
    $ruser = GetLoggedUser();
    $data['title'] = 'Prakiraan Cuaca';
    $data['edit'] = true;

    $data['data'] = $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_BMKG_TFORECAST)->row_array();
    if (empty($rdata)) {
        show_404();
        return;
    }

    if (!empty($_POST)) {
      $data['data'] = $_POST;
      $alertToday = $this->input->post("FC_Alert");
      $alertTomorrow = $this->input->post("FCT_Alert");
      if(!empty($alertToday)) $alertToday = explode(' - ', $alertToday);
      if(!empty($alertTomorrow)) $alertTomorrow = explode(' - ', $alertTomorrow);

      $rec = array(
        COL_TANGGAL=>$this->input->post(COL_TANGGAL),
        COL_ALERT_FROM=>!empty($alertToday)?reset($alertToday):null,
        COL_ALERT_TO=>!empty($alertToday)?end($alertToday):null,
        COL_ALERT_TEXT=>$this->input->post(COL_ALERT_TEXT),
        COL_ALERT_WARNING=>$this->input->post(COL_ALERT_WARNING),
        COL_ALERT_TFROM=>!empty($alertTomorrow)?reset($alertTomorrow):null,
        COL_ALERT_TTO=>!empty($alertTomorrow)?end($alertTomorrow):null,
        COL_ALERT_TTEXT=>$this->input->post(COL_ALERT_TTEXT),
        COL_ALERT_TWARNING=>$this->input->post(COL_ALERT_TWARNING),

        COL_FC1_KD_WEATHER=>$this->input->post(COL_FC1_KD_WEATHER),
        COL_FC1_DESC=>$this->input->post(COL_FC1_DESC),
        COL_FC2_KD_WEATHER=>$this->input->post(COL_FC2_KD_WEATHER),
        COL_FC2_DESC=>$this->input->post(COL_FC2_DESC),
        COL_FC3_KD_WEATHER=>$this->input->post(COL_FC3_KD_WEATHER),
        COL_FC3_DESC=>$this->input->post(COL_FC3_DESC),
        COL_FC4_KD_WEATHER=>$this->input->post(COL_FC4_KD_WEATHER),
        COL_FC4_DESC=>$this->input->post(COL_FC4_DESC),
        COL_FCT1_KD_WEATHER=>$this->input->post(COL_FCT1_KD_WEATHER),
        COL_FCT1_DESC=>$this->input->post(COL_FCT1_DESC),
        COL_FCT2_KD_WEATHER=>$this->input->post(COL_FCT2_KD_WEATHER),
        COL_FCT2_DESC=>$this->input->post(COL_FCT2_DESC),
        COL_FCT3_KD_WEATHER=>$this->input->post(COL_FCT3_KD_WEATHER),
        COL_FCT3_DESC=>$this->input->post(COL_FCT3_DESC),
        COL_FCT4_KD_WEATHER=>$this->input->post(COL_FCT4_KD_WEATHER),
        COL_FCT4_DESC=>$this->input->post(COL_FCT4_DESC),


        COL_EDIT_BY=>$ruser[COL_USERNAME],
        COL_EDIT_DATE=>date('Y-m-d H:i:s')
      );

      if(!empty($rdata[COL_FC1_IMAGES]) && !empty($_FILES['file_fc1']['name'][0])) {
        $arrImg = json_decode($rdata[COL_FC1_IMAGES]);
        foreach($arrImg as $img) {
          if(file_exists(MY_UPLOADPATH.$img)) unlink(MY_UPLOADPATH.$img);
        }
      }
      if(!empty($rdata[COL_FC2_IMAGES]) && !empty($_FILES['file_fc2']['name'][0])) {
        $arrImg = json_decode($rdata[COL_FC2_IMAGES]);
        foreach($arrImg as $img) {
          if(file_exists(MY_UPLOADPATH.$img)) unlink(MY_UPLOADPATH.$img);
        }
      }
      if(!empty($rdata[COL_FC3_IMAGES]) && !empty($_FILES['file_fc3']['name'][0])) {
        $arrImg = json_decode($rdata[COL_FC3_IMAGES]);
        foreach($arrImg as $img) {
          if(file_exists(MY_UPLOADPATH.$img)) unlink(MY_UPLOADPATH.$img);
        }
      }
      if(!empty($rdata[COL_FC4_IMAGES]) && !empty($_FILES['file_fc4']['name'][0])) {
        $arrImg = json_decode($rdata[COL_FC4_IMAGES]);
        foreach($arrImg as $img) {
          if(file_exists(MY_UPLOADPATH.$img)) unlink(MY_UPLOADPATH.$img);
        }
      }
      if(!empty($rdata[COL_FCT1_IMAGES]) && !empty($_FILES['file_fct1']['name'][0])) {
        $arrImg = json_decode($rdata[COL_FCT1_IMAGES]);
        foreach($arrImg as $img) {
          if(file_exists(MY_UPLOADPATH.$img)) unlink(MY_UPLOADPATH.$img);
        }
      }
      if(!empty($rdata[COL_FCT2_IMAGES]) && !empty($_FILES['file_fct2']['name'][0])) {
        $arrImg = json_decode($rdata[COL_FCT2_IMAGES]);
        foreach($arrImg as $img) {
          if(file_exists(MY_UPLOADPATH.$img)) unlink(MY_UPLOADPATH.$img);
        }
      }
      if(!empty($rdata[COL_FCT3_IMAGES]) && !empty($_FILES['file_fct3']['name'][0])) {
        $arrImg = json_decode($rdata[COL_FCT3_IMAGES]);
        foreach($arrImg as $img) {
          if(file_exists(MY_UPLOADPATH.$img)) unlink(MY_UPLOADPATH.$img);
        }
      }
      if(!empty($rdata[COL_FCT4_IMAGES]) && !empty($_FILES['file_fct4']['name'][0])) {
        $arrImg = json_decode($rdata[COL_FCT4_IMAGES]);
        foreach($arrImg as $img) {
          if(file_exists(MY_UPLOADPATH.$img)) unlink(MY_UPLOADPATH.$img);
        }
      }

      $config['upload_path'] = MY_UPLOADPATH;
      $config['allowed_types'] = "gif|jpg|jpeg|png";
      $config['max_size']	= 512000;
      $config['max_width']  = 4000;
      $config['max_height']  = 4000;
      $config['overwrite'] = FALSE;
      $this->load->library('upload',$config);

      $fileFC = count($_FILES['file_fc1']['name']);
      $uploadFC = array();
      if($fileFC > 0 && !empty($_FILES['file_fc1']['name'][0])) {
        for($i = 0; $i < $fileFC; $i++) {
          $_FILES['file']['name'] = $_FILES['file_fc1']['name'][$i];
          $_FILES['file']['type'] = $_FILES['file_fc1']['type'][$i];
          $_FILES['file']['tmp_name'] = $_FILES['file_fc1']['tmp_name'][$i];
          $_FILES['file']['error'] = $_FILES['file_fc1']['error'][$i];
          $_FILES['file']['size'] = $_FILES['file_fc1']['size'][$i];

          if($this->upload->do_upload('file')){
            $fileData = $this->upload->data();
            $uploadFC[] = $fileData['file_name'];
          }
          else {
            $data['upload_errors'] = $this->upload->display_errors();
            $this->template->load('backend', 'data/form_forecast', $data);
            return;
          }
        }
        $rec[COL_FC1_IMAGES] = !empty($uploadFC)?json_encode($uploadFC):null;
      }

      $fileFC = count($_FILES['file_fc2']['name']);
      $uploadFC = array();
      if($fileFC > 0 && !empty($_FILES['file_fc2']['name'][0])) {
        for($i = 0; $i < $fileFC; $i++) {
          $_FILES['file']['name'] = $_FILES['file_fc2']['name'][$i];
          $_FILES['file']['type'] = $_FILES['file_fc2']['type'][$i];
          $_FILES['file']['tmp_name'] = $_FILES['file_fc2']['tmp_name'][$i];
          $_FILES['file']['error'] = $_FILES['file_fc2']['error'][$i];
          $_FILES['file']['size'] = $_FILES['file_fc2']['size'][$i];

          if($this->upload->do_upload('file')){
            $fileData = $this->upload->data();
            $uploadFC[] = $fileData['file_name'];
          }
          else {
            $data['upload_errors'] = $this->upload->display_errors();
            $this->template->load('backend', 'data/form_forecast', $data);
            return;
          }
        }
        $rec[COL_FC2_IMAGES] = !empty($uploadFC)?json_encode($uploadFC):null;
      }

      $fileFC = count($_FILES['file_fc3']['name']);
      $uploadFC = array();
      if($fileFC > 0 && !empty($_FILES['file_fc3']['name'][0])) {
        for($i = 0; $i < $fileFC; $i++) {
          $_FILES['file']['name'] = $_FILES['file_fc3']['name'][$i];
          $_FILES['file']['type'] = $_FILES['file_fc3']['type'][$i];
          $_FILES['file']['tmp_name'] = $_FILES['file_fc3']['tmp_name'][$i];
          $_FILES['file']['error'] = $_FILES['file_fc3']['error'][$i];
          $_FILES['file']['size'] = $_FILES['file_fc3']['size'][$i];

          if($this->upload->do_upload('file')){
            $fileData = $this->upload->data();
            $uploadFC[] = $fileData['file_name'];
          }
          else {
            $data['upload_errors'] = $this->upload->display_errors();
            $this->template->load('backend', 'data/form_forecast', $data);
            return;
          }
        }
        $rec[COL_FC3_IMAGES] = !empty($uploadFC)?json_encode($uploadFC):null;
      }

      $fileFC = count($_FILES['file_fc4']['name']);
      $uploadFC = array();
      if($fileFC > 0 && !empty($_FILES['file_fc4']['name'][0])) {
        for($i = 0; $i < $fileFC; $i++) {
          $_FILES['file']['name'] = $_FILES['file_fc4']['name'][$i];
          $_FILES['file']['type'] = $_FILES['file_fc4']['type'][$i];
          $_FILES['file']['tmp_name'] = $_FILES['file_fc4']['tmp_name'][$i];
          $_FILES['file']['error'] = $_FILES['file_fc4']['error'][$i];
          $_FILES['file']['size'] = $_FILES['file_fc4']['size'][$i];

          if($this->upload->do_upload('file')){
            $fileData = $this->upload->data();
            $uploadFC[] = $fileData['file_name'];
          }
          else {
            $data['upload_errors'] = $this->upload->display_errors();
            $this->template->load('backend', 'data/form_forecast', $data);
            return;
          }
        }
        $rec[COL_FC4_IMAGES] = !empty($uploadFC)?json_encode($uploadFC):null;
      }

      $fileFC = count($_FILES['file_fct1']['name']);
      $uploadFC = array();
      if($fileFC > 0 && !empty($_FILES['file_fct1']['name'][0])) {
        for($i = 0; $i < $fileFC; $i++) {
          $_FILES['file']['name'] = $_FILES['file_fct1']['name'][$i];
          $_FILES['file']['type'] = $_FILES['file_fct1']['type'][$i];
          $_FILES['file']['tmp_name'] = $_FILES['file_fct1']['tmp_name'][$i];
          $_FILES['file']['error'] = $_FILES['file_fct1']['error'][$i];
          $_FILES['file']['size'] = $_FILES['file_fct1']['size'][$i];

          if($this->upload->do_upload('file')){
            $fileData = $this->upload->data();
            $uploadFC[] = $fileData['file_name'];
          }
          else {
            $data['upload_errors'] = $this->upload->display_errors();
            $this->template->load('backend', 'data/form_forecast', $data);
            return;
          }
        }
        $rec[COL_FCT1_IMAGES] = !empty($uploadFC)?json_encode($uploadFC):null;
      }

      $fileFC = count($_FILES['file_fct2']['name']);
      $uploadFC = array();
      if($fileFC > 0 && !empty($_FILES['file_fct2']['name'][0])) {
        for($i = 0; $i < $fileFC; $i++) {
          $_FILES['file']['name'] = $_FILES['file_fct2']['name'][$i];
          $_FILES['file']['type'] = $_FILES['file_fct2']['type'][$i];
          $_FILES['file']['tmp_name'] = $_FILES['file_fct2']['tmp_name'][$i];
          $_FILES['file']['error'] = $_FILES['file_fct2']['error'][$i];
          $_FILES['file']['size'] = $_FILES['file_fct2']['size'][$i];

          if($this->upload->do_upload('file')){
            $fileData = $this->upload->data();
            $uploadFC[] = $fileData['file_name'];
          }
          else {
            $data['upload_errors'] = $this->upload->display_errors();
            $this->template->load('backend', 'data/form_forecast', $data);
            return;
          }
        }
        $rec[COL_FCT2_IMAGES] = !empty($uploadFC)?json_encode($uploadFC):null;
      }

      $fileFC = count($_FILES['file_fct3']['name']);
      $uploadFC = array();
      if($fileFC > 0 && !empty($_FILES['file_fct3']['name'][0])) {
        for($i = 0; $i < $fileFC; $i++) {
          $_FILES['file']['name'] = $_FILES['file_fct3']['name'][$i];
          $_FILES['file']['type'] = $_FILES['file_fct3']['type'][$i];
          $_FILES['file']['tmp_name'] = $_FILES['file_fct3']['tmp_name'][$i];
          $_FILES['file']['error'] = $_FILES['file_fct3']['error'][$i];
          $_FILES['file']['size'] = $_FILES['file_fct3']['size'][$i];

          if($this->upload->do_upload('file')){
            $fileData = $this->upload->data();
            $uploadFC[] = $fileData['file_name'];
          }
          else {
            $data['upload_errors'] = $this->upload->display_errors();
            $this->template->load('backend', 'data/form_forecast', $data);
            return;
          }
        }
        $rec[COL_FCT3_IMAGES] = !empty($uploadFC)?json_encode($uploadFC):null;
      }

      $fileFC = count($_FILES['file_fct4']['name']);
      $uploadFC = array();
      if($fileFC > 0 && !empty($_FILES['file_fct4']['name'][0])) {
        for($i = 0; $i < $fileFC; $i++) {
          $_FILES['file']['name'] = $_FILES['file_fct4']['name'][$i];
          $_FILES['file']['type'] = $_FILES['file_fct4']['type'][$i];
          $_FILES['file']['tmp_name'] = $_FILES['file_fct4']['tmp_name'][$i];
          $_FILES['file']['error'] = $_FILES['file_fct4']['error'][$i];
          $_FILES['file']['size'] = $_FILES['file_fct4']['size'][$i];

          if($this->upload->do_upload('file')){
            $fileData = $this->upload->data();
            $uploadFC[] = $fileData['file_name'];
          }
          else {
            $data['upload_errors'] = $this->upload->display_errors();
            $this->template->load('backend', 'data/form_forecast', $data);
            return;
          }
        }
        $rec[COL_FCT4_IMAGES] = !empty($uploadFC)?json_encode($uploadFC):null;
      }

      $res = $this->db->where(COL_UNIQ, $id)->update(TBL_BMKG_TFORECAST, $rec);
      if ($res) {
          redirect('site/data/forecast-index');
      } else {
          $this->template->load('backend', 'data/form_forecast', $data);
      }
    } else {
      $this->template->load('backend', 'data/form_forecast', $data);
    }
  }

  public function forecast_delete()
  {
    $data = $this->input->post('cekbox');
    $deleted = 0;
    foreach ($data as $datum) {
        $this->db->delete(TBL_BMKG_TFORECAST, array(COL_UNIQ => $datum, "ifnull(Published,'') != " => 1));
        $deleted++;
    }
    if ($deleted) {
        ShowJsonSuccess($deleted." data dihapus");
    } else {
        ShowJsonError("Tidak ada dihapus");
    }
  }

  public function forecast_publish($pub=1)
  {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLESUPERVISOR) {
      ShowJsonError("Anda tidak memiliki hak akses.");
      return;
    }
    $data = $this->input->post('cekbox');
    $deleted = 0;
    foreach ($data as $datum) {
        $this->db->where(COL_UNIQ, $datum)->update(TBL_BMKG_TFORECAST, array(COL_PUBLISHED => ($pub==1?1:null)));
        $deleted++;
    }
    if ($deleted) {
        ShowJsonSuccess($deleted." data dipublish");
    } else {
        ShowJsonError("Tidak ada dipublish");
    }
  }

  public function forecast_pending() {
    $this->db->select('
    bmkg_tforecast.*,
    w1.Nm_Icon_Day as Ic_FC1,
    w2.Nm_Icon_Day as Ic_FC2,
    w3.Nm_Icon_Day as Ic_FC3,
    w4.Nm_Icon_Day as Ic_FC4,
    w1.Nm_Weather as Nm_FC1,
    w2.Nm_Weather as Nm_FC2,
    w3.Nm_Weather as Nm_FC3,
    w4.Nm_Weather as Nm_FC4,
    wt1.Nm_Icon_Day as Ic_FCT1,
    wt2.Nm_Icon_Day as Ic_FCT2,
    wt3.Nm_Icon_Day as Ic_FCT3,
    wt4.Nm_Icon_Day as Ic_FCT4,
    wt1.Nm_Weather as Nm_FCT1,
    wt2.Nm_Weather as Nm_FCT2,
    wt3.Nm_Weather as Nm_FCT3,
    wt4.Nm_Weather as Nm_FCT4');
    $this->db->join(TBL_BMKG_MWEATHER.' w1', 'w1.Kd_Weather = bmkg_tforecast.FC1_Kd_Weather', 'left');
    $this->db->join(TBL_BMKG_MWEATHER.' w2', 'w2.Kd_Weather = bmkg_tforecast.FC2_Kd_Weather', 'left');
    $this->db->join(TBL_BMKG_MWEATHER.' w3', 'w3.Kd_Weather = bmkg_tforecast.FC3_Kd_Weather', 'left');
    $this->db->join(TBL_BMKG_MWEATHER.' w4', 'w4.Kd_Weather = bmkg_tforecast.FC4_Kd_Weather', 'left');
    $this->db->join(TBL_BMKG_MWEATHER.' wt1', 'wt1.Kd_Weather = bmkg_tforecast.FCT1_Kd_Weather', 'left');
    $this->db->join(TBL_BMKG_MWEATHER.' wt2', 'wt2.Kd_Weather = bmkg_tforecast.FCT2_Kd_Weather', 'left');
    $this->db->join(TBL_BMKG_MWEATHER.' wt3', 'wt3.Kd_Weather = bmkg_tforecast.FCT3_Kd_Weather', 'left');
    $this->db->join(TBL_BMKG_MWEATHER.' wt4', 'wt4.Kd_Weather = bmkg_tforecast.FCT4_Kd_Weather', 'left');
    $data['res'] = $this->db
    ->where('(Published is null or Published != 1)')
    ->where(COL_TANGGAL.' >= ', date('Y-m-d'))
    ->order_by(COL_TANGGAL, 'desc')->get(TBL_BMKG_TFORECAST)->result_array();
    $this->load->view('data/index_forecast_pending', $data);
  }

  public function info_index($type='') {
    $data['title'] = "Informasi";
    $data['data'] = '';
    $data['type'] = $type;
    $this->template->load('backend', 'data/index_earthquake', $data);
  }

  public function info_index_load($type='') {
    $orderdef = array(COL_TANGGAL=>'desc');
    $orderables = array(null,COL_TANGGAL, COL_PUBLISHED, COL_CREATE_BY, COL_CREATE_BY);
    $cols = array(COL_TANGGAL, COL_PUBLISHED, COL_CREATE_BY, COL_CREATE_BY);

    $i = 0;
    foreach($cols as $item){
      if($_POST['search']['value']){
        if($i===0){
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        }else{
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(isset($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(isset($orderdef)){
        $order = $orderdef;
        $this->db->order_by(key($order), $order[key($order)]);
    }

    $this->db->select('bmkg_tearthquake.*');
    $query = $this->db->where(COL_NM_TYPE, $type)->get(TBL_BMKG_TEARTHQUAKE);
    $data = [];

    foreach($query->result_array() as $r) {
      $txtDetail = 'Magnitude: '.$r[COL_NUM_MAGNITUDE].'<br />Kedalaman: '.$r[COL_NUM_DEPTH].'<br />Lat: '.$r[COL_NUM_LAT].'<br />Long: '.$r[COL_NUM_LONG];
      $data[] = array(
        '<input type="checkbox" class="cekbox" name="cekbox[]" value="' . $r[COL_UNIQ] . '" />',
        anchor('site/data/info-edit/'.$r[COL_UNIQ], date('Y-m-d', strtotime($r[COL_TANGGAL]))),
        //$txtDetail,
        $r[COL_NM_DESC],
        $r[COL_NM_WARNING],
        $r[COL_PUBLISHED]?'<i class="fad fa-lg fa-check-square text-success"></i>':'<i class="fad fa-lg fa-times-square text-danger"></i>',
        $r[COL_CREATE_BY],
        date('Y-m-d', strtotime($r[COL_CREATE_DATE])),
      );
    }


    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $query->num_rows(),
      "recordsTotal" => $this->db->count_all_results(TBL_BMKG_TEARTHQUAKE),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function info_add($type='') {
    $ruser = GetLoggedUser();
    $data['title'] = 'Informasi';
    $data['edit'] = false;
    $data['type'] = $type;
    if (!empty($_POST)) {
      $data['data'] = $_POST;
      $config['upload_path'] = MY_UPLOADPATH;
      $config['allowed_types'] = "gif|jpg|jpeg|png|mp4|mpeg|mkv|3gp";
      $config['max_size']	= 512000;
      $config['max_width']  = 4000;
      $config['max_height']  = 4000;
      $config['overwrite'] = FALSE;
      $this->load->library('upload',$config);

      $rec = array(
        COL_TANGGAL=>$this->input->post(COL_TANGGAL),
        COL_NM_TYPE=>$type,
        COL_NUM_MAGNITUDE=>$this->input->post(COL_NUM_MAGNITUDE),
        COL_NUM_DEPTH=>$this->input->post(COL_NUM_DEPTH),
        COL_NUM_LAT=>$this->input->post(COL_NUM_LAT),
        COL_NUM_LONG=>$this->input->post(COL_NUM_LONG),
        COL_NM_DESC=>$this->input->post(COL_NM_DESC),
        COL_NM_WARNING=>$this->input->post(COL_NM_WARNING),

        COL_CREATE_BY=>$ruser[COL_USERNAME],
        COL_CREATE_DATE=>date('Y-m-d H:i:s')
      );

      $fileFC = count($_FILES['files']['name']);
      $uploadFC = array();
      if($fileFC > 0 && !empty($_FILES['files']['name'][0])) {
        for($i = 0; $i < $fileFC; $i++) {
          $_FILES['file']['name'] = $_FILES['files']['name'][$i];
          $_FILES['file']['type'] = $_FILES['files']['type'][$i];
          $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
          $_FILES['file']['error'] = $_FILES['files']['error'][$i];
          $_FILES['file']['size'] = $_FILES['files']['size'][$i];

          if($this->upload->do_upload('file')){
            $fileData = $this->upload->data();
            $uploadFC[] = $fileData['file_name'];
          }
          else {
            $data['upload_errors'] = $this->upload->display_errors();
            $this->template->load('backend', 'data/form_earthquake', $data);
            return;
          }
        }
        $rec[COL_IMAGES] = !empty($uploadFC)?json_encode($uploadFC):null;
      }

      $res = $this->db->insert(TBL_BMKG_TEARTHQUAKE, $rec);
      if ($res) {
          redirect('site/data/info-index/'.$type);
      } else {
        $data['err'] = $this->db->error();
        $this->template->load('backend', 'data/form_earthquake', $data);
      }

    } else {
      $this->template->load('backend', 'data/form_earthquake', $data);
    }
  }

  public function info_edit($id) {
    $ruser = GetLoggedUser();
    $data['title'] = 'Informasi';
    $data['edit'] = true;

    $data['data'] = $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_BMKG_TEARTHQUAKE)->row_array();
    if (empty($rdata)) {
        show_404();
        return;
    }
    $data['type'] = $rdata[COL_NM_TYPE];

    if (!empty($_POST)) {
      $data['data'] = $_POST;
      $rec = array(
        COL_TANGGAL=>$this->input->post(COL_TANGGAL),
        COL_NUM_MAGNITUDE=>$this->input->post(COL_NUM_MAGNITUDE),
        COL_NUM_DEPTH=>$this->input->post(COL_NUM_DEPTH),
        COL_NUM_LAT=>$this->input->post(COL_NUM_LAT),
        COL_NUM_LONG=>$this->input->post(COL_NUM_LONG),
        COL_NM_DESC=>$this->input->post(COL_NM_DESC),
        COL_NM_WARNING=>$this->input->post(COL_NM_WARNING),

        COL_EDIT_BY=>$ruser[COL_USERNAME],
        COL_EDIT_DATE=>date('Y-m-d H:i:s')
      );

      if(!empty($rdata[COL_NM_IMAGES]) && !empty($_FILES['files']['name'][0])) {
        $arrImg = json_decode($rdata[COL_NM_IMAGES]);
        foreach($arrImg as $img) {
          if(file_exists(MY_UPLOADPATH.$img)) unlink(MY_UPLOADPATH.$img);
        }
      }

      $config['upload_path'] = MY_UPLOADPATH;
      $config['allowed_types'] = "gif|jpg|jpeg|png";
      $config['max_size']	= 512000;
      $config['max_width']  = 4000;
      $config['max_height']  = 4000;
      $config['overwrite'] = FALSE;
      $this->load->library('upload',$config);

      $fileFC = count($_FILES['files']['name']);
      $uploadFC = array();
      if($fileFC > 0 && !empty($_FILES['files']['name'][0])) {
        for($i = 0; $i < $fileFC; $i++) {
          $_FILES['file']['name'] = $_FILES['files']['name'][$i];
          $_FILES['file']['type'] = $_FILES['files']['type'][$i];
          $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
          $_FILES['file']['error'] = $_FILES['files']['error'][$i];
          $_FILES['file']['size'] = $_FILES['files']['size'][$i];

          if($this->upload->do_upload('file')){
            $fileData = $this->upload->data();
            $uploadFC[] = $fileData['file_name'];
          }
          else {
            $data['upload_errors'] = $this->upload->display_errors();
            $this->template->load('backend', 'data/form_earthquake', $data);
            return;
          }
        }
        $rec[COL_NM_IMAGES] = !empty($uploadFC)?json_encode($uploadFC):null;
      }

      $res = $this->db->where(COL_UNIQ, $id)->update(TBL_BMKG_TEARTHQUAKE, $rec);
      if ($res) {
          redirect('site/data/info-index/'.$rdata[COL_NM_TYPE]);
      } else {
          $this->template->load('backend', 'data/form_earthquake', $data);
      }
    } else {
      $this->template->load('backend', 'data/form_earthquake', $data);
    }
  }

  public function info_delete()
  {
    $data = $this->input->post('cekbox');
    $deleted = 0;
    foreach ($data as $datum) {
        $this->db->delete(TBL_BMKG_TEARTHQUAKE, array(COL_UNIQ => $datum, "ifnull(Published,'') != " => 1));
        $deleted++;
    }
    if ($deleted) {
        ShowJsonSuccess($deleted.' data dihapus.');
    } else {
        ShowJsonError("Tidak ada dihapus");
    }
  }

  public function info_publish($pub=1)
  {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLESUPERVISOR) {
      ShowJsonError("Anda tidak memiliki hak akses.");
      return;
    }
    $data = $this->input->post('cekbox');
    $deleted = 0;
    foreach ($data as $datum) {
        $this->db->where(COL_UNIQ, $datum)->update(TBL_BMKG_TEARTHQUAKE, array(COL_PUBLISHED => ($pub==1?1:null)));
        $deleted++;
    }
    if ($deleted) {
        ShowJsonSuccess($deleted." data dipublish");
    } else {
        ShowJsonError("Tidak ada dipublish");
    }
  }

  public function info_pending() {
    $this->db->select('
    bmkg_tforecast.*,
    w1.Nm_Icon_Day as Ic_FC1,
    w2.Nm_Icon_Day as Ic_FC2,
    w3.Nm_Icon_Day as Ic_FC3,
    w4.Nm_Icon_Day as Ic_FC4,
    w1.Nm_Weather as Nm_FC1,
    w2.Nm_Weather as Nm_FC2,
    w3.Nm_Weather as Nm_FC3,
    w4.Nm_Weather as Nm_FC4,
    wt1.Nm_Icon_Day as Ic_FCT1,
    wt2.Nm_Icon_Day as Ic_FCT2,
    wt3.Nm_Icon_Day as Ic_FCT3,
    wt4.Nm_Icon_Day as Ic_FCT4,
    wt1.Nm_Weather as Nm_FCT1,
    wt2.Nm_Weather as Nm_FCT2,
    wt3.Nm_Weather as Nm_FCT3,
    wt4.Nm_Weather as Nm_FCT4');
    $this->db->join(TBL_BMKG_MWEATHER.' w1', 'w1.Kd_Weather = bmkg_tforecast.FC1_Kd_Weather', 'left');
    $this->db->join(TBL_BMKG_MWEATHER.' w2', 'w2.Kd_Weather = bmkg_tforecast.FC2_Kd_Weather', 'left');
    $this->db->join(TBL_BMKG_MWEATHER.' w3', 'w3.Kd_Weather = bmkg_tforecast.FC3_Kd_Weather', 'left');
    $this->db->join(TBL_BMKG_MWEATHER.' w4', 'w4.Kd_Weather = bmkg_tforecast.FC4_Kd_Weather', 'left');
    $this->db->join(TBL_BMKG_MWEATHER.' wt1', 'wt1.Kd_Weather = bmkg_tforecast.FCT1_Kd_Weather', 'left');
    $this->db->join(TBL_BMKG_MWEATHER.' wt2', 'wt2.Kd_Weather = bmkg_tforecast.FCT2_Kd_Weather', 'left');
    $this->db->join(TBL_BMKG_MWEATHER.' wt3', 'wt3.Kd_Weather = bmkg_tforecast.FCT3_Kd_Weather', 'left');
    $this->db->join(TBL_BMKG_MWEATHER.' wt4', 'wt4.Kd_Weather = bmkg_tforecast.FCT4_Kd_Weather', 'left');
    $data['res'] = $this->db
    ->where('(Published is null or Published != 1)')
    ->where(COL_TANGGAL.' >= ', date('Y-m-d'))
    ->order_by(COL_TANGGAL, 'desc')->get(TBL_BMKG_TFORECAST)->result_array();
    $this->load->view('data/index_forecast_pending', $data);
  }

  public function report() {
    $data['title'] = 'Generate Laporan';
    $data['data'] = array();
    $data['result'] = array();
    $data['page'] = 'all';
    if(!empty($_GET)) {
      $data['data'] = $_GET;
      $cetak = $this->input->get('CETAK');

      $this->db->select('
      bmkg_tforecast.*,
      w1.Nm_Icon_Day as Ic_FC1,
      w2.Nm_Icon_Day as Ic_FC2,
      w3.Nm_Icon_Day as Ic_FC3,
      w4.Nm_Icon_Day as Ic_FC4,
      w1.Nm_Weather as Nm_FC1,
      w2.Nm_Weather as Nm_FC2,
      w3.Nm_Weather as Nm_FC3,
      w4.Nm_Weather as Nm_FC4,
      wt1.Nm_Icon_Day as Ic_FCT1,
      wt2.Nm_Icon_Day as Ic_FCT2,
      wt3.Nm_Icon_Day as Ic_FCT3,
      wt4.Nm_Icon_Day as Ic_FCT4,
      wt1.Nm_Weather as Nm_FCT1,
      wt2.Nm_Weather as Nm_FCT2,
      wt3.Nm_Weather as Nm_FCT3,
      wt4.Nm_Weather as Nm_FCT4');
      $this->db->join(TBL_BMKG_MWEATHER.' w1', 'w1.Kd_Weather = bmkg_tforecast.FC1_Kd_Weather', 'left');
      $this->db->join(TBL_BMKG_MWEATHER.' w2', 'w2.Kd_Weather = bmkg_tforecast.FC2_Kd_Weather', 'left');
      $this->db->join(TBL_BMKG_MWEATHER.' w3', 'w3.Kd_Weather = bmkg_tforecast.FC3_Kd_Weather', 'left');
      $this->db->join(TBL_BMKG_MWEATHER.' w4', 'w4.Kd_Weather = bmkg_tforecast.FC4_Kd_Weather', 'left');
      $this->db->join(TBL_BMKG_MWEATHER.' wt1', 'wt1.Kd_Weather = bmkg_tforecast.FCT1_Kd_Weather', 'left');
      $this->db->join(TBL_BMKG_MWEATHER.' wt2', 'wt2.Kd_Weather = bmkg_tforecast.FCT2_Kd_Weather', 'left');
      $this->db->join(TBL_BMKG_MWEATHER.' wt3', 'wt3.Kd_Weather = bmkg_tforecast.FCT3_Kd_Weather', 'left');
      $this->db->join(TBL_BMKG_MWEATHER.' wt4', 'wt4.Kd_Weather = bmkg_tforecast.FCT4_Kd_Weather', 'left');

      $this->db->where(COL_TANGGAL, $this->input->get(COL_TANGGAL));
      $this->db->where(COL_PUBLISHED, 1);
      $data['res'] = $this->db->get(TBL_BMKG_TFORECAST)->row_array();

      if(!empty($cetak)) {
        $this->load->library('Mypdf');

        $mpdf = new Mypdf('', 'A4-L');
        $html = $this->load->view('data/report_partial', $data, TRUE);
        //echo $html;
        //return;
        $mpdf->pdf->SetTitle('Info BMKG');
        $mpdf->pdf->shrink_tables_to_fit = 1;
        $mpdf->pdf->WriteHTML($html);
        $mpdf->pdf->Output('Info BMKG.pdf', 'I');
        return;
      }
    }
    $this->template->load('backend', 'data/report', $data);
  }
}
 ?>
