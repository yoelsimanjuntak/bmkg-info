<?php
$ruser = GetLoggedUser();
$today = $this->db->where(COL_TANGGAL, date('Y-m-d'))->get(TBL_BMKG_TFORECAST)->row_array();

$this->db->select('
bmkg_tforecast.*,
w1.Nm_Icon_Day as Ic_FC1,
w2.Nm_Icon_Day as Ic_FC2,
w3.Nm_Icon_Day as Ic_FC3,
w4.Nm_Icon_Day as Ic_FC4,
w1.Nm_Weather as Nm_FC1,
w2.Nm_Weather as Nm_FC2,
w3.Nm_Weather as Nm_FC3,
w4.Nm_Weather as Nm_FC4,
wt1.Nm_Icon_Day as Ic_FCT1,
wt2.Nm_Icon_Day as Ic_FCT2,
wt3.Nm_Icon_Day as Ic_FCT3,
wt4.Nm_Icon_Day as Ic_FCT4,
wt1.Nm_Weather as Nm_FCT1,
wt2.Nm_Weather as Nm_FCT2,
wt3.Nm_Weather as Nm_FCT3,
wt4.Nm_Weather as Nm_FCT4');
$this->db->join(TBL_BMKG_MWEATHER.' w1', 'w1.Kd_Weather = bmkg_tforecast.FC1_Kd_Weather', 'left');
$this->db->join(TBL_BMKG_MWEATHER.' w2', 'w2.Kd_Weather = bmkg_tforecast.FC2_Kd_Weather', 'left');
$this->db->join(TBL_BMKG_MWEATHER.' w3', 'w3.Kd_Weather = bmkg_tforecast.FC3_Kd_Weather', 'left');
$this->db->join(TBL_BMKG_MWEATHER.' w4', 'w4.Kd_Weather = bmkg_tforecast.FC4_Kd_Weather', 'left');
$this->db->join(TBL_BMKG_MWEATHER.' wt1', 'wt1.Kd_Weather = bmkg_tforecast.FCT1_Kd_Weather', 'left');
$this->db->join(TBL_BMKG_MWEATHER.' wt2', 'wt2.Kd_Weather = bmkg_tforecast.FCT2_Kd_Weather', 'left');
$this->db->join(TBL_BMKG_MWEATHER.' wt3', 'wt3.Kd_Weather = bmkg_tforecast.FCT3_Kd_Weather', 'left');
$this->db->join(TBL_BMKG_MWEATHER.' wt4', 'wt4.Kd_Weather = bmkg_tforecast.FCT4_Kd_Weather', 'left');
$rrecent = $this->db->order_by(COL_TANGGAL, 'desc')->limit(10)->get(TBL_BMKG_TFORECAST)->result_array();
 ?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark"><?=$title?></h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>">Home</a></li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <?php
      if(empty($today) && $ruser[COL_ROLEID] != ROLESUPERVISOR) {
        ?>
        <div class="col-sm-12">
          <div class="callout callout-danger">
            <h6><i class="fad fa-exclamation-circle text-danger"></i>&nbsp;PRAKIRAAN CUACA BELUM TERSEDIA</h6>
            <p>Data Prakiraan Cuaca hari ini (<strong><?=date('d-m-Y')?></strong>) belum tersedia untuk ditayangkan / ditampilkan. Harap mengisi Prakiraan Cuaca Hari ini pada menu yang disediakan.</p>
            <p class="text-right text-white">
              <?=anchor('site/data/forecast-add', '<i class="fas fa-plus"></i> Prakiraan Cuaca Hari Ini', array('class'=>'btn btn-danger btn-sm text-white','style'=>'text-decoration:none'))?>
            </p>
          </div>
        </div>
        <?php
      }
      ?>
      <?php
      if($today && $today[COL_PUBLISHED] != 1 && $ruser[COL_ROLEID] != ROLESTAFF) {
        ?>
        <div class="col-sm-12">
          <div class="callout callout-warning">
            <h6><i class="fad fa-exclamation-circle text-warning"></i>&nbsp;PRAKIRAAN CUACA BELUM TAYANG</h6>
            <p>Data Prakiraan Cuaca hari ini (<strong><?=date('d-m-Y')?></strong>) belum tersedia untuk ditayangkan / ditampilkan. Harap update status <strong>PUBLISH</strong> pada Prakiraan Cuaca Hari di menu yang disediakan.</p>
            <p class="text-right text-white">
              <?=anchor('site/data/forecast-index', '<i class="fad fa-list"></i> Data Selengkapnya', array('class'=>'btn btn-warning btn-sm','style'=>'text-decoration:none'))?>
            </p>
          </div>
        </div>
        <?php
      }
      ?>
      <div class="col-sm-6">
        <div id="card-pending" class="card card-outline card-warning">
          <div class="card-header">
            <h3 class="card-title">Prakiraan Cuaca</h3>
            <div class="card-tools">
              <span class="badge badge-warning">BELUM TAYANG</span>
            </div>
          </div>
          <div class="card-body p-0">

          </div>
          <div class="overlay" style="display: none">
            <i class="fad fa-2x fa-sync-alt fa-spin"></i>
          </div>
          <div class="card-footer text-right">
            <a href="<?=site_url('site/data/forecast-publish')?>" class="btn btn-success btn-sm btn-publish-pending" data-confirm="Tayangkan data ini?"><i class="fas fa-check"></i>&nbsp;PUBLISH</a>
            <button type="button" class="btn btn-default btn-sm btn-refresh-pending"><i class="fas fa-sync-alt"></i>&nbsp;REFRESH</button>
          </div>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="card card-outline card-primary">
          <div class="card-header">
            <h3 class="card-title">Prakiraan Cuaca</h3>
            <div class="card-tools">
              <span class="badge badge-primary">TERKINI</span>
            </div>
          </div>
          <div class="card-body p-0">
            <table class="table table-striped">
              <thead class="text-center">
                <tr>
                  <th>Tanggal</th>
                  <th>Hari Ini</th>
                  <th>Besok</th>
                  <th>Publish</th>
                  <th>Oleh</th>
                </tr>
              </thead>
              <tbody class="text-center">
                <?php
                foreach($rrecent as $r) {
                  ?>
                  <tr>
                    <td><?=anchor('site/data/forecast-edit/'.$r[COL_UNIQ], date('Y-m-d', strtotime($r[COL_TANGGAL])))?></td>
                    <td>
                      <?=
                      '<i class="fad fa-'.(!empty($r['Ic_FC1'])?$r['Ic_FC1']:'question-circle').'" title="Pagi: '.(!empty($r['Nm_FC1'])?$r['Nm_FC1']:'').'"></i>&nbsp'.
                      '<i class="fad fa-'.(!empty($r['Ic_FC2'])?$r['Ic_FC2']:'question-circle').'" title="Siang: '.(!empty($r['Nm_FC2'])?$r['Nm_FC2']:'').'"></i>&nbsp'.
                      '<i class="fad fa-'.(!empty($r['Ic_FC3'])?$r['Ic_FC3']:'question-circle').'" title="Sore: '.(!empty($r['Nm_FC3'])?$r['Nm_FC3']:'').'"></i>&nbsp'.
                      '<i class="fad fa-'.(!empty($r['Ic_FC4'])?$r['Ic_FC4']:'question-circle').'" title="Malam: '.(!empty($r['Nm_FC4'])?$r['Nm_FC4']:'').'"></i>'
                      ?>
                    </td>
                    <td>
                      <?=
                      '<i class="fad fa-'.(!empty($r['Ic_FCT1'])?$r['Ic_FCT1']:'question-circle').'" title="Pagi: '.(!empty($r['Nm_FCT1'])?$r['Nm_FCT1']:'').'"></i>&nbsp'.
                      '<i class="fad fa-'.(!empty($r['Ic_FCT2'])?$r['Ic_FCT2']:'question-circle').'" title="Siang: '.(!empty($r['Nm_FCT2'])?$r['Nm_FCT2']:'').'"></i>&nbsp'.
                      '<i class="fad fa-'.(!empty($r['Ic_FCT3'])?$r['Ic_FCT3']:'question-circle').'" title="Sore: '.(!empty($r['Nm_FCT3'])?$r['Nm_FCT3']:'').'"></i>&nbsp'.
                      '<i class="fad fa-'.(!empty($r['Ic_FCT4'])?$r['Ic_FCT4']:'question-circle').'" title="Malam: '.(!empty($r['Nm_FCT4'])?$r['Nm_FCT4']:'').'"></i>'
                      ?>
                    </td>
                    <td><?=$r[COL_PUBLISHED]?'<i class="fad fa-check-square text-success"></i>':'<i class="fad fa-times-square text-danger"></i>'?></td>
                    <td><?=$r[COL_CREATE_BY]?></td>
                  </tr>
                  <?php
                }
                 ?>
              </tbody>
            </table>
          </div>
          <div class="card-footer text-right">
            <a href="<?=site_url('site/data/forecast-index')?>" class="btn btn-primary btn-sm"><i class="fad fa-list"></i>&nbsp;SELENGKAPNYA</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script>
$(document).ready(function() {
  $('.btn-refresh-pending', $('#card-pending')).unbind().click(function() {
    var dis = $(this);
    dis.html('<i class="fas fa-sync-alt fa-spin"></i>&nbsp;LOADING...');
    $('.overlay', $('#card-pending')).show();
    $('.card-body', $('#card-pending')).load('<?=site_url('site/data/forecast-pending')?>', function() {
      $('.overlay', $('#card-pending')).hide();
      dis.html('<i class="fas fa-sync-alt"></i>&nbsp;REFRESH');
    })
  }).trigger('click');

  $('.btn-publish-pending', $('#card-pending')).unbind().click(function(){
    var a = $(this);
    var confirmDialog = $("#confirmDialog");
    var alertDialog = $("#alertDialog");

    if($('.checkbox-pending-item:checked').length < 1){
        $(".modal-body", alertDialog).html("Belum ada data dipilih.");
        alertDialog.modal("show");
        return false;
    }

    $(".modal-body", confirmDialog).html((a.data('confirm')||"Are you sure?"));
    confirmDialog.modal("show");
    $(".btn-ok", confirmDialog).click(function() {
      var thisBtn = $(this);
        thisBtn.html("Loading...").attr("disabled", true);
        $('#dataform-pending', $('#card-pending')).ajaxSubmit({
          dataType: 'json',
          url : a.attr('href'),
          success : function(data){
            if(data.error==0){
              toastr.success(data.success);
              $('.btn-refresh-pending', $('#card-pending')).click();
            }else{
              toastr.error(data.error);
            }
          },
          complete: function(){
            thisBtn.html("OK").attr("disabled", false);
            confirmDialog.modal("hide");
          }
        });
    });
    return false;
  });
});
</script>
