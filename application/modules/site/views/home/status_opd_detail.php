<div class="content-header">
    <!--<div class="container">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark"><?= $title ?></h1>
            </div>
        </div>
    </div>-->
</div>
<div class="content">
  <div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header with-border">
                    <h4 class="card-title"><a href="<?=site_url('site/home/status-opd')?>" class="btn btn-default"><i class="fa fa-chevron-left"></i>&nbsp;&nbsp;KEMBALI</a></h4>
                </div>
                <div class="card-body p-0">
                  <form id="dataform" method="post" action="#">
                    <table id="datalist" class="table table-bordered table-hover mb-0">
                      <thead>
                        <tr>
                          <th class="border-top-0 text-center" rowspan="2">Unit</th>
                          <th class="border-top-0 text-center" colspan="2">Renstra</th>
                          <th class="border-top-0 text-center" colspan="2">Renja</th>
                          <th class="border-top-0 text-center" colspan="2">Cascading</th>
                          <th class="border-top-0 text-center" colspan="2">DPA</th>
                        </tr>
                        <tr>
                          <th class="border-top-0 text-center">Tujuan</th>
                          <th class="border-top-0 text-center">Sasaran</th>
                          <th class="border-top-0 text-center">Program</th>
                          <th class="border-top-0 text-center">Kegiatan</th>
                          <th class="border-top-0 text-center">Es. III</th>
                          <th class="border-top-0 text-center">Es. IV</th>
                          <th class="border-top-0 text-center">Program</th>
                          <th class="border-top-0 text-center">Kegiatan</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td class="text-bold"><?=$res[COL_NM_SUB_UNIT]?></td>
                          <td class="text-right"><?=number_format($res["Tujuan"], 0)?></td>
                          <td class="text-right"><?=number_format($res["Sasaran"], 0)?></td>
                          <td colspan="6"></td>
                        </tr>
                        <?php
                        $q = @"
                        select
                        bid.*,
                        (
                          select count(*) from `sakip_mbid_program` prg
                          where
                            prg.Kd_Pemda = $KdPemda
                            and prg.Kd_Tahun = $Tahun
                            and prg.Kd_Urusan = bid.Kd_Urusan
                            and prg.Kd_Bidang = bid.Kd_Bidang
                            and prg.Kd_Unit = bid.Kd_Unit
                            and prg.Kd_Sub = bid.Kd_Sub
                            and prg.Kd_Bid = bid.Kd_Bid
                        ) as Program,
                        (
                        	select count(*) from `sakip_msubbid_kegiatan` keg
                        	where
                        		keg.Kd_Pemda = $KdPemda
                        		and keg.Kd_Tahun = $Tahun
                        		and keg.Kd_Urusan = bid.Kd_Urusan
                        		and keg.Kd_Bidang = bid.Kd_Bidang
                        		and keg.Kd_Unit = bid.Kd_Unit
                        		and keg.Kd_Sub = bid.Kd_Sub
                            and keg.Kd_Bid = bid.Kd_Bid
                        ) as Kegiatan,
                        (
                          select count(*) from `sakip_mbid_sasaran` sasaranbid
                          where
                            sasaranbid.Kd_Pemda = $KdPemda
                            and sasaranbid.Kd_Urusan = bid.Kd_Urusan
                            and sasaranbid.Kd_Bidang = bid.Kd_Bidang
                            and sasaranbid.Kd_Unit = bid.Kd_Unit
                            and sasaranbid.Kd_Sub = bid.Kd_Sub
                            and sasaranbid.Kd_Bid = bid.Kd_Bid
                        ) as Cascading_Es3,
                        (
                        	select count(*) from `sakip_msubbid_sasaran` sasaransub
                        	where
                        		sasaransub.Kd_Pemda = $KdPemda
                        		and sasaransub.Kd_Urusan = bid.Kd_Urusan
                        		and sasaransub.Kd_Bidang = bid.Kd_Bidang
                        		and sasaransub.Kd_Unit = bid.Kd_Unit
                        		and sasaransub.Kd_Sub = bid.Kd_Sub
                            and sasaransub.Kd_Bid = bid.Kd_Bid
                        ) as Cascading_Es4,
                        (
                          select count(*) from `sakip_dpa_program` dpaprog
                          where
                            dpaprog.Kd_Pemda = $KdPemda
                            and dpaprog.Kd_Tahun = $Tahun
                            and dpaprog.Kd_Urusan = bid.Kd_Urusan
                            and dpaprog.Kd_Bidang = bid.Kd_Bidang
                            and dpaprog.Kd_Unit = bid.Kd_Unit
                            and dpaprog.Kd_Sub = bid.Kd_Sub
                            and dpaprog.Kd_Bid = bid.Kd_Bid
                        ) as DPA_Prg,
                        (
                        	select count(*) from `sakip_dpa_kegiatan` dpakeg
                        	where
                        		dpakeg.Kd_Pemda = $KdPemda
                        		and dpakeg.Kd_Tahun = $Tahun
                        		and dpakeg.Kd_Urusan = bid.Kd_Urusan
                        		and dpakeg.Kd_Bidang = bid.Kd_Bidang
                        		and dpakeg.Kd_Unit = bid.Kd_Unit
                        		and dpakeg.Kd_Sub = bid.Kd_Sub
                            and dpakeg.Kd_Bid = bid.Kd_Bid
                        ) as DPA_Keg
                        from sakip_mbid bid
                        where
                          bid.Kd_Urusan = $kdUrusan
                          and bid.Kd_Bidang = $kdBidang
                          and bid.Kd_Unit = $kdUnit
                          and bid.Kd_Sub = $kdSub
                        ";
                        $rbidang = $this->db->query($q)->result_array();
                        foreach($rbidang as $bid) {
                          ?>
                          <tr>
                            <td style="text-indent: 2em;" colspan="3"><?=strtoupper($bid[COL_NM_BID])?></td>
                            <td class="text-right"><?=number_format($bid["Program"], 0)?></td>
                            <td class="text-right"><?=number_format($bid["Kegiatan"], 0)?></td>
                            <td class="text-right"><?=number_format($bid["Cascading_Es3"], 0)?></td>
                            <td class="text-right"><?=number_format($bid["Cascading_Es4"], 0)?></td>
                            <td class="text-right"><?=number_format($bid["DPA_Prg"], 0)?></td>
                            <td class="text-right"><?=number_format($bid["DPA_Keg"], 0)?></td>
                          </tr>
                          <?php
                          $kdBid = $bid[COL_KD_BID];
                          $q = @"
                          select
                          subbid.*,
                          (
                          	select count(*) from `sakip_msubbid_kegiatan` keg
                          	where
                          		keg.Kd_Pemda = $KdPemda
                          		and keg.Kd_Tahun = $Tahun
                          		and keg.Kd_Urusan = subbid.Kd_Urusan
                          		and keg.Kd_Bidang = subbid.Kd_Bidang
                          		and keg.Kd_Unit = subbid.Kd_Unit
                          		and keg.Kd_Sub = subbid.Kd_Sub
                              and keg.Kd_Bid = subbid.Kd_Bid
                              and keg.Kd_Subbid = subbid.Kd_Subbid
                          ) as Kegiatan,
                          (
                          	select count(*) from `sakip_msubbid_sasaran` sasaransub
                          	where
                          		sasaransub.Kd_Pemda = $KdPemda
                          		and sasaransub.Kd_Urusan = subbid.Kd_Urusan
                          		and sasaransub.Kd_Bidang = subbid.Kd_Bidang
                          		and sasaransub.Kd_Unit = subbid.Kd_Unit
                          		and sasaransub.Kd_Sub = subbid.Kd_Sub
                              and sasaransub.Kd_Bid = subbid.Kd_Bid
                              and sasaransub.Kd_Subbid = subbid.Kd_Subbid
                          ) as Cascading_Es4,
                          (
                          	select count(*) from `sakip_dpa_kegiatan` dpakeg
                          	where
                          		dpakeg.Kd_Pemda = $KdPemda
                          		and dpakeg.Kd_Tahun = $Tahun
                          		and dpakeg.Kd_Urusan = subbid.Kd_Urusan
                          		and dpakeg.Kd_Bidang = subbid.Kd_Bidang
                          		and dpakeg.Kd_Unit = subbid.Kd_Unit
                          		and dpakeg.Kd_Sub = subbid.Kd_Sub
                              and dpakeg.Kd_Bid = subbid.Kd_Bid
                              and dpakeg.Kd_Subbid = subbid.Kd_Subbid
                          ) as DPA_Keg
                          from sakip_msubbid subbid
                          where
                            subbid.Kd_Urusan = $kdUrusan
                            and subbid.Kd_Bidang = $kdBidang
                            and subbid.Kd_Unit = $kdUnit
                            and subbid.Kd_Sub = $kdSub
                            and subbid.Kd_Bid = $kdBid
                          ";
                          $rsubbidang = $this->db->query($q)->result_array();
                          foreach($rsubbidang as $sbid) {
                            ?>
                            <tr>
                              <td style="text-indent: 4em;" colspan="4"><?=strtoupper($sbid[COL_NM_SUBBID])?></td>
                              <td class="text-right"><?=number_format($sbid["Kegiatan"], 0)?></td>
                              <td class="text-right"></td>
                              <td class="text-right"><?=number_format($sbid["Cascading_Es4"], 0)?></td>
                              <td class="text-right"></td>
                              <td class="text-right"><?=number_format($sbid["DPA_Keg"], 0)?></td>
                            </tr>
                            <?php
                          }
                        }
                         ?>
                      </tbody>
                    </table>
                  </form>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>
