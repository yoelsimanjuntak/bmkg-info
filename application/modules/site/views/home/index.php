<style>
.todo-list>li:hover {
    background-color: #ccc;
}
.carousel-container {
  min-height: 300px;
}
.products-list .product-description {
  text-overflow: inherit;
  white-space: inherit;
}
.timeline::before {
  display: none;
}
</style>
<div class="content-header">
    <!--<div class="container">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark"><?= $title ?></h1>
            </div>
        </div>
    </div>-->
</div>
<div class="content">
  <div class="container">
    <div class="row">
      <div class="col-lg-7">
        <div class="card">
          <div class="card-header d-flex p-0">
            <h3 class="card-title p-3">Prakiraan Cuaca</h3>
            <ul class="nav nav-pills ml-auto p-2">
              <li class="nav-item"><a class="nav-link active" href="#today" data-toggle="tab">HARI INI <small><?=date('d-m-Y')?></small></a></li>
              <li class="nav-item"><a class="nav-link" href="#tomorrow" data-toggle="tab">BESOK <small><?=date('d-m-Y', strtotime(date('Y-m-d').'+1 days'))?></small></a></li>
              <li class="nav-item"><a class="nav-link" href="#weekly" data-toggle="tab">MINGGUAN</a></li>
              <!--<li class="nav-item dropdown show">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="true">
                  Dropdown <span class="caret"></span>
                </a>
                <div class="dropdown-menu show" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-5px, 40px, 0px);">
                  <a class="dropdown-item" tabindex="-1" href="#">Action</a>
                  <a class="dropdown-item" tabindex="-1" href="#">Another action</a>
                  <a class="dropdown-item" tabindex="-1" href="#">Something else here</a>
                  <div class="divider"></div>
                  <a class="dropdown-item" tabindex="-1" href="#">Separated link</a>
                </div>
              </li>-->
            </ul>
          </div>
          <div class="card-body">
            <?php
            if(empty($res) && false) {
              ?>
              <div class="callout callout-danger">
                <span class="text-danger"><i class="fad fa-search"></i>&nbsp;&nbsp;Belum ada informasi prakiraan cuaca hari ini.</span>
              </div>
              <?php
            } else {
              ?>
              <div class="tab-content">
                <div class="tab-pane active" id="today">
                  <?php
                  if(empty($res)) {
                    ?>
                    <div class="callout callout-danger">
                      <span class="text-danger"><i class="fad fa-search"></i>&nbsp;&nbsp;Belum ada informasi prakiraan cuaca hari ini.</span>
                    </div>
                    <?php
                  } else {
                    ?>
                    <div id="accordionToday">
                      <div class="card card-outline card-info">
                        <div class="card-header">
                          <h4 class="card-title">
                            <a class="text-info" data-toggle="collapse" data-parent="#accordionToday" href="#todayPagi">
                              <i class="fad fa-sunrise"></i> PAGI
                            </a>
                          </h4>
                        </div>
                        <div id="todayPagi" class="panel-collapse in collapse show">
                          <div class="card-body">
                            <h4 class="text-center">
                              <i class="fad fa-<?=$res['Ic_FC1']?>"></i>&nbsp;<?=$res['Nm_FC1']?>
                            </h4>
                            <p class="m-0 text-justify">
                              <?=$res[COL_FC1_DESC]?>
                            </p>
                          </div>
                          <div class="card-footer pt-0 bg-white text-center">
                            <?php
                            if(!empty($res[COL_FC1_IMAGES])) {
                              $arr_imgFC1 = json_decode($res[COL_FC1_IMAGES]);
                              if(!empty($arr_imgFC1) && count($arr_imgFC1) > 0) {
                                ?>

                                <?php
                                for($i=0; $i<count($arr_imgFC1); $i++) {
                                  ?>
                                  <a href="<?=MY_UPLOADURL.$arr_imgFC1[$i]?>" data-toggle="lightbox" data-title="Gambar" data-gallery="gallery">
                                    <img src="<?=MY_UPLOADURL.$arr_imgFC1[$i]?>" class="elevation-2 mb-2" width="300px" style="max-width: 100%" />
                                  </a>
                                  <?php
                                }
                                ?>
                                <!--<div id="carouselTodayPagi" class="carousel slide" data-ride="carousel">
                                  <ol class="carousel-indicators">
                                    <?php
                                    for($i=0; $i<count($arr_imgFC1); $i++) {
                                      ?>
                                      <li data-target="#carouselTodayPagi" data-slide-to="<?=$i?>" <?=$i==0?'class="active"':''?>></li>
                                      <?php
                                    }
                                    ?>
                                  </ol>
                                  <div class="carousel-inner">
                                    <?php
                                    for($i=0; $i<count($arr_imgFC1); $i++) {
                                      ?>
                                      <div class="carousel-item <?=$i==0?'active':''?>">
                                        <div class="carousel-container" style="background: url('<?=MY_UPLOADURL.$arr_imgFC1[$i]?>'); background-size: cover"></div>
                                      </div>
                                      <?php
                                    }
                                    ?>
                                  </div>
                                  <a class="carousel-control-prev" href="#carouselTodayPagi" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                  </a>
                                  <a class="carousel-control-next" href="#carouselTodayPagi" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                  </a>
                                </div>-->
                                <?php
                              }
                              ?>
                              <?php
                            }
                             ?>
                          </div>
                        </div>
                      </div>

                      <div class="card card-outline card-warning">
                        <div class="card-header">
                          <h4 class="card-title">
                            <a class="text-warning" data-toggle="collapse" data-parent="#accordionToday" href="#todaySiang">
                              <i class="fad fa-sun"></i> SIANG
                            </a>
                          </h4>
                        </div>
                        <div id="todaySiang" class="panel-collapse in collapse">
                          <div class="card-body">
                            <h4 class="text-center">
                              <i class="fad fa-<?=$res['Ic_FC2']?>"></i>&nbsp;<?=$res['Nm_FC2']?>
                            </h4>
                            <p class="m-0 text-justify">
                              <?=$res[COL_FC2_DESC]?>
                            </p>
                          </div>
                          <div class="card-footer pt-0 bg-white text-center">
                            <?php
                            if(!empty($res[COL_FC2_IMAGES])) {
                              $arr_imgFC1 = json_decode($res[COL_FC2_IMAGES]);
                              if(!empty($arr_imgFC1) && count($arr_imgFC1) > 0) {
                                ?>
                                <?php
                                for($i=0; $i<count($arr_imgFC1); $i++) {
                                  ?>
                                  <a href="<?=MY_UPLOADURL.$arr_imgFC1[$i]?>" data-toggle="lightbox" data-title="Gambar" data-gallery="gallery">
                                    <img src="<?=MY_UPLOADURL.$arr_imgFC1[$i]?>" class="elevation-2 mb-2" width="300px" style="max-width: 100%" />
                                  </a>
                                  <?php
                                }
                                ?>
                                <!--<div id="carouselTodaySiang" class="carousel slide" data-ride="carousel">
                                  <ol class="carousel-indicators">
                                    <?php
                                    for($i=0; $i<count($arr_imgFC1); $i++) {
                                      ?>
                                      <li data-target="#carouselTodaySiang" data-slide-to="<?=$i?>" <?=$i==0?'class="active"':''?>></li>
                                      <?php
                                    }
                                    ?>
                                  </ol>
                                  <div class="carousel-inner">
                                    <?php
                                    for($i=0; $i<count($arr_imgFC1); $i++) {
                                      ?>
                                      <div class="carousel-item <?=$i==0?'active':''?>">
                                        <div class="carousel-container" style="background: url('<?=MY_UPLOADURL.$arr_imgFC1[$i]?>'); background-size: cover"></div>
                                      </div>
                                      <?php
                                    }
                                    ?>
                                  </div>
                                  <a class="carousel-control-prev" href="#carouselTodaySiang" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                  </a>
                                  <a class="carousel-control-next" href="#carouselTodaySiang" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                  </a>
                                </div>-->
                                <?php
                              }
                              ?>
                              <?php
                            }
                             ?>
                          </div>
                        </div>
                      </div>

                      <div class="card card-outline card-danger">
                        <div class="card-header">
                          <h4 class="card-title">
                            <a class="text-danger" data-toggle="collapse" data-parent="#accordionToday" href="#todaySore">
                              <i class="fad fa-sunset"></i> SORE
                            </a>
                          </h4>
                        </div>
                        <div id="todaySore" class="panel-collapse in collapse">
                          <div class="card-body">
                            <h4 class="text-center">
                              <i class="fad fa-<?=$res['Ic_FC3']?>"></i>&nbsp;<?=$res['Nm_FC3']?>
                            </h4>
                            <p class="m-0 text-justify">
                              <?=$res[COL_FC3_DESC]?>
                            </p>
                          </div>
                          <div class="card-footer pt-0 bg-white text-center">
                            <?php
                            if(!empty($res[COL_FC3_IMAGES])) {
                              $arr_imgFC1 = json_decode($res[COL_FC3_IMAGES]);
                              if(!empty($arr_imgFC1) && count($arr_imgFC1) > 0) {
                                ?>

                                <?php
                                for($i=0; $i<count($arr_imgFC1); $i++) {
                                  ?>
                                  <a href="<?=MY_UPLOADURL.$arr_imgFC1[$i]?>" data-toggle="lightbox" data-title="Gambar" data-gallery="gallery">
                                    <img src="<?=MY_UPLOADURL.$arr_imgFC1[$i]?>" class="elevation-2 mb-2" width="300px" style="max-width: 100%" />
                                  </a>
                                  <?php
                                }
                                ?>

                                <!--<div id="carouselTodaySore" class="carousel slide" data-ride="carousel">
                                  <ol class="carousel-indicators">
                                    <?php
                                    for($i=0; $i<count($arr_imgFC1); $i++) {
                                      ?>
                                      <li data-target="#carouselTodaySore" data-slide-to="<?=$i?>" <?=$i==0?'class="active"':''?>></li>
                                      <?php
                                    }
                                    ?>
                                  </ol>
                                  <div class="carousel-inner">
                                    <?php
                                    for($i=0; $i<count($arr_imgFC1); $i++) {
                                      ?>
                                      <div class="carousel-item <?=$i==0?'active':''?>">
                                        <div class="carousel-container" style="background: url('<?=MY_UPLOADURL.$arr_imgFC1[$i]?>'); background-size: cover"></div>
                                      </div>
                                      <?php
                                    }
                                    ?>
                                  </div>
                                  <a class="carousel-control-prev" href="#carouselTodaySore" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                  </a>
                                  <a class="carousel-control-next" href="#carouselTodaySore" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                  </a>
                                </div>-->
                                <?php
                              }
                              ?>
                              <?php
                            }
                             ?>
                          </div>
                        </div>
                      </div>

                      <div class="card card-outline card-gray">
                        <div class="card-header">
                          <h4 class="card-title">
                            <a class="text-gray" data-toggle="collapse" data-parent="#accordionToday" href="#todayMalam">
                              <i class="fad fa-moon"></i> MALAM
                            </a>
                          </h4>
                        </div>
                        <div id="todayMalam" class="panel-collapse in collapse">
                          <div class="card-body">
                            <h4 class="text-center">
                              <i class="fad fa-<?=$res['Ic_FC4']?>"></i>&nbsp;<?=$res['Nm_FC4']?>
                            </h4>
                            <p class="m-0 text-justify">
                              <?=$res[COL_FC4_DESC]?>
                            </p>
                          </div>
                          <div class="card-footer pt-0 bg-white text-center">
                            <?php
                            if(!empty($res[COL_FC4_IMAGES])) {
                              $arr_imgFC1 = json_decode($res[COL_FC4_IMAGES]);
                              if(!empty($arr_imgFC1) && count($arr_imgFC1) > 0) {
                                ?>

                                <?php
                                for($i=0; $i<count($arr_imgFC1); $i++) {
                                  ?>
                                  <a href="<?=MY_UPLOADURL.$arr_imgFC1[$i]?>" data-toggle="lightbox" data-title="Gambar" data-gallery="gallery">
                                    <img src="<?=MY_UPLOADURL.$arr_imgFC1[$i]?>" class="elevation-2 mb-2" width="300px" style="max-width: 100%" />
                                  </a>
                                  <?php
                                }
                                ?>

                                <!--<div id="carouselTodayMalam" class="carousel slide" data-ride="carousel">
                                  <ol class="carousel-indicators">
                                    <?php
                                    for($i=0; $i<count($arr_imgFC1); $i++) {
                                      ?>
                                      <li data-target="#carouselTodayMalam" data-slide-to="<?=$i?>" <?=$i==0?'class="active"':''?>></li>
                                      <?php
                                    }
                                    ?>
                                  </ol>
                                  <div class="carousel-inner">
                                    <?php
                                    for($i=0; $i<count($arr_imgFC1); $i++) {
                                      ?>
                                      <div class="carousel-item <?=$i==0?'active':''?>">
                                        <div class="carousel-container" style="background: url('<?=MY_UPLOADURL.$arr_imgFC1[$i]?>'); background-size: cover"></div>
                                      </div>
                                      <?php
                                    }
                                    ?>
                                  </div>
                                  <a class="carousel-control-prev" href="#carouselTodayMalam" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                  </a>
                                  <a class="carousel-control-next" href="#carouselTodayMalam" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                  </a>
                                </div>-->
                                <?php
                              }
                              ?>
                              <?php
                            }
                             ?>
                          </div>
                        </div>
                      </div>
                    </div>

                    <?php
                    if(!empty($res[COL_ALERT_TEXT])) {
                      ?>
                      <div class="callout callout-warning">
                        <h5><i class="fad fa-exclamation-triangle text-warning"></i>&nbsp;&nbsp;HIMBAUAN</h5>
                        <p class="text-justify"><?=$res[COL_ALERT_TEXT]?></p>
                      </div>
                      <?php
                    }
                    ?>
                    <?php
                    if(!empty($res[COL_ALERT_WARNING])) {
                      ?>
                      <div class="callout callout-danger">
                        <h5><i class="fad fa-engine-warning text-danger"></i>&nbsp;&nbsp;PERINGATAN</h5>
                        <p class="text-justify"><?=$res[COL_ALERT_WARNING]?></p>
                      </div>
                      <?php
                    }
                  }
                  ?>
                </div>
                <div class="tab-pane" id="tomorrow">
                  <?php
                  if(empty($res)) {
                    ?>
                    <div class="callout callout-danger">
                      <span class="text-danger"><i class="fad fa-search"></i>&nbsp;&nbsp;Belum ada informasi prakiraan cuaca hari ini.</span>
                    </div>
                    <?php
                  } else {
                    ?>
                    <div id="accordionTomorrow">
                      <div class="card card-outline card-info">
                        <div class="card-header">
                          <h4 class="card-title">
                            <a class="text-info" data-toggle="collapse" data-parent="#accordionTomorrow" href="#tomorrowPagi">
                              <i class="fad fa-sunrise"></i> PAGI
                            </a>
                          </h4>
                        </div>
                        <div id="tomorrowPagi" class="panel-collapse in collapse show">
                          <div class="card-body">
                            <h4 class="text-center">
                              <i class="fad fa-<?=$res['Ic_FCT1']?>"></i>&nbsp;<?=$res['Nm_FCT1']?>
                            </h4>
                            <p class="m-0 text-justify">
                              <?=$res[COL_FCT1_DESC]?>
                            </p>
                          </div>
                          <div class="card-footer pt-0 bg-white text-center">
                            <?php
                            if(!empty($res[COL_FCT1_IMAGES])) {
                              $arr_imgFC1 = json_decode($res[COL_FCT1_IMAGES]);
                              if(!empty($arr_imgFC1) && count($arr_imgFC1) > 0) {
                                ?>

                                <?php
                                for($i=0; $i<count($arr_imgFC1); $i++) {
                                  ?>
                                  <a href="<?=MY_UPLOADURL.$arr_imgFC1[$i]?>" data-toggle="lightbox" data-title="Gambar" data-gallery="gallery">
                                    <img src="<?=MY_UPLOADURL.$arr_imgFC1[$i]?>" class="elevation-2 mb-2" width="300px" style="max-width: 100%" />
                                  </a>
                                  <?php
                                }
                                ?>

                                <!--<div id="carouselTomorrowPagi" class="carousel slide" data-ride="carousel">
                                  <ol class="carousel-indicators">
                                    <?php
                                    for($i=0; $i<count($arr_imgFC1); $i++) {
                                      ?>
                                      <li data-target="#carouselTomorrowPagi" data-slide-to="<?=$i?>" <?=$i==0?'class="active"':''?>></li>
                                      <?php
                                    }
                                    ?>
                                  </ol>
                                  <div class="carousel-inner">
                                    <?php
                                    for($i=0; $i<count($arr_imgFC1); $i++) {
                                      ?>
                                      <div class="carousel-item <?=$i==0?'active':''?>">
                                        <div class="carousel-container" style="background: url('<?=MY_UPLOADURL.$arr_imgFC1[$i]?>'); background-size: cover"></div>
                                      </div>
                                      <?php
                                    }
                                    ?>
                                  </div>
                                  <a class="carousel-control-prev" href="#carouselTomorrowPagi" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                  </a>
                                  <a class="carousel-control-next" href="#carouselTomorrowPagi" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                  </a>
                                </div>-->
                                <?php
                              }
                              ?>
                              <?php
                            }
                             ?>
                          </div>
                        </div>
                      </div>

                      <div class="card card-outline card-warning">
                        <div class="card-header">
                          <h4 class="card-title">
                            <a class="text-warning" data-toggle="collapse" data-parent="#accordionTomorrow" href="#tomorrowSiang">
                              <i class="fad fa-sun"></i> SIANG
                            </a>
                          </h4>
                        </div>
                        <div id="tomorrowSiang" class="panel-collapse in collapse">
                          <div class="card-body">
                            <h4 class="text-center">
                              <i class="fad fa-<?=$res['Ic_FCT2']?>"></i>&nbsp;<?=$res['Nm_FCT2']?>
                            </h4>
                            <p class="m-0 text-justify">
                              <?=$res[COL_FCT2_DESC]?>
                            </p>
                          </div>
                          <div class="card-footer pt-0 bg-white text-center">
                            <?php
                            if(!empty($res[COL_FCT2_IMAGES])) {
                              $arr_imgFC1 = json_decode($res[COL_FCT2_IMAGES]);
                              if(!empty($arr_imgFC1) && count($arr_imgFC1) > 0) {
                                ?>

                                <?php
                                for($i=0; $i<count($arr_imgFC1); $i++) {
                                  ?>
                                  <a href="<?=MY_UPLOADURL.$arr_imgFC1[$i]?>" data-toggle="lightbox" data-title="Gambar" data-gallery="gallery">
                                    <img src="<?=MY_UPLOADURL.$arr_imgFC1[$i]?>" class="elevation-2 mb-2" width="300px" style="max-width: 100%" />
                                  </a>
                                  <?php
                                }
                                ?>

                                <!--<div id="carouselTomorrowSiang" class="carousel slide" data-ride="carousel">
                                  <ol class="carousel-indicators">
                                    <?php
                                    for($i=0; $i<count($arr_imgFC1); $i++) {
                                      ?>
                                      <li data-target="#carouselTomorrowSiang" data-slide-to="<?=$i?>" <?=$i==0?'class="active"':''?>></li>
                                      <?php
                                    }
                                    ?>
                                  </ol>
                                  <div class="carousel-inner">
                                    <?php
                                    for($i=0; $i<count($arr_imgFC1); $i++) {
                                      ?>
                                      <div class="carousel-item <?=$i==0?'active':''?>">
                                        <div class="carousel-container" style="background: url('<?=MY_UPLOADURL.$arr_imgFC1[$i]?>'); background-size: cover"></div>
                                      </div>
                                      <?php
                                    }
                                    ?>
                                  </div>
                                  <a class="carousel-control-prev" href="#carouselTomorrowSiang" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                  </a>
                                  <a class="carousel-control-next" href="#carouselTomorrowSiang" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                  </a>
                                </div>-->
                                <?php
                              }
                              ?>
                              <?php
                            }
                             ?>
                          </div>
                        </div>
                      </div>

                      <div class="card card-outline card-danger">
                        <div class="card-header">
                          <h4 class="card-title">
                            <a class="text-danger" data-toggle="collapse" data-parent="#accordionTomorrow" href="#tomorrowSore">
                              <i class="fad fa-sunset"></i> SORE
                            </a>
                          </h4>
                        </div>
                        <div id="tomorrowSore" class="panel-collapse in collapse">
                          <div class="card-body">
                            <h4 class="text-center">
                              <i class="fad fa-<?=$res['Ic_FCT3']?>"></i>&nbsp;<?=$res['Nm_FCT3']?>
                            </h4>
                            <p class="m-0 text-justify">
                              <?=$res[COL_FCT3_DESC]?>
                            </p>
                          </div>
                          <div class="card-footer pt-0 bg-white text-center">
                            <?php
                            if(!empty($res[COL_FCT3_IMAGES])) {
                              $arr_imgFC1 = json_decode($res[COL_FCT3_IMAGES]);
                              if(!empty($arr_imgFC1) && count($arr_imgFC1) > 0) {
                                ?>

                                <?php
                                for($i=0; $i<count($arr_imgFC1); $i++) {
                                  ?>
                                  <a href="<?=MY_UPLOADURL.$arr_imgFC1[$i]?>" data-toggle="lightbox" data-title="Gambar" data-gallery="gallery">
                                    <img src="<?=MY_UPLOADURL.$arr_imgFC1[$i]?>" class="elevation-2 mb-2" width="300px" style="max-width: 100%" />
                                  </a>
                                  <?php
                                }
                                ?>

                                <!--<div id="carouselTomorrowSore" class="carousel slide" data-ride="carousel">
                                  <ol class="carousel-indicators">
                                    <?php
                                    for($i=0; $i<count($arr_imgFC1); $i++) {
                                      ?>
                                      <li data-target="#carouselTomorrowSore" data-slide-to="<?=$i?>" <?=$i==0?'class="active"':''?>></li>
                                      <?php
                                    }
                                    ?>
                                  </ol>
                                  <div class="carousel-inner">
                                    <?php
                                    for($i=0; $i<count($arr_imgFC1); $i++) {
                                      ?>
                                      <div class="carousel-item <?=$i==0?'active':''?>">
                                        <div class="carousel-container" style="background: url('<?=MY_UPLOADURL.$arr_imgFC1[$i]?>'); background-size: cover"></div>
                                      </div>
                                      <?php
                                    }
                                    ?>
                                  </div>
                                  <a class="carousel-control-prev" href="#carouselTomorrowSore" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                  </a>
                                  <a class="carousel-control-next" href="#carouselTomorrowSore" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                  </a>
                                </div>-->
                                <?php
                              }
                              ?>
                              <?php
                            }
                             ?>
                          </div>
                        </div>
                      </div>

                      <div class="card card-outline card-gray">
                        <div class="card-header">
                          <h4 class="card-title">
                            <a class="text-gray" data-toggle="collapse" data-parent="#accordionTomorrow" href="#tomorrowMalam">
                              <i class="fad fa-moon"></i> MALAM
                            </a>
                          </h4>
                        </div>
                        <div id="tomorrowMalam" class="panel-collapse in collapse">
                          <div class="card-body">
                            <h4 class="text-center">
                              <i class="fad fa-<?=$res['Ic_FCT4']?>"></i>&nbsp;<?=$res['Nm_FCT4']?>
                            </h4>
                            <p class="m-0 text-justify">
                              <?=$res[COL_FCT4_DESC]?>
                            </p>
                          </div>
                          <div class="card-footer pt-0 bg-white text-center">
                            <?php
                            if(!empty($res[COL_FCT4_IMAGES])) {
                              $arr_imgFC1 = json_decode($res[COL_FCT4_IMAGES]);
                              if(!empty($arr_imgFC1) && count($arr_imgFC1) > 0) {
                                ?>

                                <?php
                                for($i=0; $i<count($arr_imgFC1); $i++) {
                                  ?>
                                  <a href="<?=MY_UPLOADURL.$arr_imgFC1[$i]?>" data-toggle="lightbox" data-title="Gambar" data-gallery="gallery">
                                    <img src="<?=MY_UPLOADURL.$arr_imgFC1[$i]?>" class="elevation-2 mb-2" width="300px" style="max-width: 100%" />
                                  </a>
                                  <?php
                                }
                                ?>

                                <!--<div id="carouselTomorrowMalam" class="carousel slide" data-ride="carousel">
                                  <ol class="carousel-indicators">
                                    <?php
                                    for($i=0; $i<count($arr_imgFC1); $i++) {
                                      ?>
                                      <li data-target="#carouselTomorrowMalam" data-slide-to="<?=$i?>" <?=$i==0?'class="active"':''?>></li>
                                      <?php
                                    }
                                    ?>
                                  </ol>
                                  <div class="carousel-inner">
                                    <?php
                                    for($i=0; $i<count($arr_imgFC1); $i++) {
                                      ?>
                                      <div class="carousel-item <?=$i==0?'active':''?>">
                                        <div class="carousel-container" style="background: url('<?=MY_UPLOADURL.$arr_imgFC1[$i]?>'); background-size: cover"></div>
                                      </div>
                                      <?php
                                    }
                                    ?>
                                  </div>
                                  <a class="carousel-control-prev" href="#carouselTomorrowMalam" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                  </a>
                                  <a class="carousel-control-next" href="#carouselTomorrowMalam" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                  </a>
                                </div>-->
                                <?php
                              }
                              ?>
                              <?php
                            }
                             ?>
                          </div>
                        </div>
                      </div>
                    </div>

                    <?php
                    if(!empty($res[COL_ALERT_TTEXT])) {
                      ?>
                      <div class="callout callout-warning">
                        <h5><i class="fad fa-exclamation-triangle text-warning"></i>&nbsp;&nbsp;HIMBAUAN</h5>
                        <p class="text-justify"><?=$res[COL_ALERT_TTEXT]?></p>
                      </div>
                      <?php
                    }
                    ?>
                    <?php
                    if(!empty($res[COL_ALERT_TWARNING])) {
                      ?>
                      <div class="callout callout-danger">
                        <h5><i class="fad fa-engine-warning text-danger"></i>&nbsp;&nbsp;PERINGATAN</h5>
                        <p class="text-justify"><?=$res[COL_ALERT_TWARNING]?></p>
                      </div>
                      <?php
                    }
                  }
                  ?>
                </div>
                <div class="tab-pane" id="weekly">
                  <?php
                  $datWeekly = $this->db
                  ->where(array(COL_NM_TYPE=>'FORECAST-WEEK', COL_PUBLISHED=>1))
                  ->where('WEEK(Tanggal) = WEEK(CURRENT_DATE())')
                  ->where('YEAR(Tanggal) = YEAR(CURRENT_DATE())')
                  ->order_by(COL_TANGGAL, 'desc')
                  ->get(TBL_BMKG_TEARTHQUAKE)
                  ->row_array();
                  if(empty($datWeekly)) {
                    echo 'Tidak ada data tersedia.';
                  } else {
                    ?>
                    <p><?=$datWeekly[COL_NM_DESC]?></p>
                    <?php
                    $imgs = json_decode($datWeekly[COL_IMAGES]);
                    if(!empty($imgs) && count($imgs) > 0) {
                      ?>
                      <div class="text-center mb-2">
                        <?php
                        foreach($imgs as $img) {
                          if(!file_exists(MY_UPLOADPATH.$img)) continue;
                          if(strpos(mime_content_type(MY_UPLOADPATH.$img), 'video') !== false) {
                            ?>
                            <video width="320" height="240" controls>
                              <source src="<?=MY_UPLOADURL.$img?>" type="<?=mime_content_type(MY_UPLOADPATH.$img)?>"  />
                              Your browser does not support the video tag.
                            </video>
                            <?php
                          } else if(strpos(mime_content_type(MY_UPLOADPATH.$img), 'image') !== false) {
                            ?>
                            <img class="m-2" src="<?=MY_UPLOADURL.$img?>" style="height: 20vh; border: 1.5px solid #dedede; box-shadow: 3px 4px 5px rgba(0,0,0,0.2);" />
                            <?php
                          } else if(strpos(mime_content_type(MY_UPLOADPATH.$img), 'application/pdf') !== false) {
                            ?>
                            <p><?=anchor(MY_UPLOADURL.$img, '<i class="fad fa-paperclip"></i>&nbsp;'.$img, array('target' => '_blank'))?></p>
                            <?php
                          }
                        }
                        ?>
                      </div>
                      <?php
                    }
                    ?>
                    <p class="text-danger">
                      <strong>HIMBAUAN:</strong><br />
                      <?=$datWeekly[COL_NM_WARNING]?>
                    </p>
                    <?php
                  }
                  ?>
                </div>
              </div>
              <?php
            }
             ?>
            <!--<div id="carouselInfoToday" class="carousel slide" data-ride="carousel">
              <ol class="carousel-indicators">
                <li data-target="#carouselInfoToday" data-slide-to="0" class="active"></li>
                <li data-target="#carouselInfoToday" data-slide-to="1"></li>
                <li data-target="#carouselInfoToday" data-slide-to="2"></li>
              </ol>
              <div class="carousel-inner">
                <div class="carousel-item">
                  <h4>Pagi</h4>
                  <table>
                    <tbody>
                      <tr>
                        <td>Keterangan</td><td>:</td>
                        <td>
                          <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                            Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                            Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                            Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                          </p>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <div class="carousel-item active">
                  Dua
                </div>
                <div class="carousel-item">
                  Tiga
                </div>
              </div>
              <a class="carousel-control-prev" href="#carouselInfoToday" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="carousel-control-next" href="#carouselInfoToday" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
            </div>-->
          </div>
          <?php
          $datEarthquake = $this->db
          ->where(array(COL_NM_TYPE=>'EARTHQUAKE', COL_TANGGAL=>date('Y-m-d'), COL_PUBLISHED=>1))
          ->get(TBL_BMKG_TEARTHQUAKE)
          ->row_array();
          $datTsunami = $this->db
          ->where(array(COL_NM_TYPE=>'TSUNAMI', COL_TANGGAL=>date('Y-m-d'), COL_PUBLISHED=>1))
          ->get(TBL_BMKG_TEARTHQUAKE)
          ->row_array();
          $datHotspot = $this->db
          ->where(array(COL_NM_TYPE=>'HOTSPOT', COL_TANGGAL=>date('Y-m-d'), COL_PUBLISHED=>1))
          ->get(TBL_BMKG_TEARTHQUAKE)
          ->row_array();

          if(!empty($datEarthquake) || !empty($datTsunami) || !empty($datHotspot)) {
            if(!empty($datEarthquake)) {
              ?>
              <div class="card-footer">
                <div class="callout callout-secondary">
                  <h5><i class="fad fa-monitor-heart-rate"></i> INFO GEMPA</h5>
                  <p><?=$datEarthquake[COL_NM_DESC]?></p>

                  <?php
                  if(!empty($datEarthquake[COL_NM_WARNING])) {
                    ?>
                    <p class="text-danger">
                      <strong>HIMBAUAN:</strong><br />
                      <?=$datEarthquake[COL_NM_WARNING]?>
                    </p>
                    <?php
                  }
                  ?>
                </div>
              </div>
              <?php
            }
            if(!empty($datTsunami)) {
              ?>
              <div class="card-footer">
                <div class="callout callout-secondary">
                  <h5><i class="fad fa-monitor-heart-rate"></i> INFO TSUNAMI</h5>
                  <p><?=$datTsunami[COL_NM_DESC]?></p>

                  <?php
                  if(!empty($datTsunami[COL_NM_WARNING])) {
                    ?>
                    <p class="text-danger">
                      <strong>HIMBAUAN:</strong><br />
                      <?=$datTsunami[COL_NM_WARNING]?>
                    </p>
                    <?php
                  }
                  ?>
                </div>
              </div>
              <?php
            }
            if(!empty($datHotspot)) {
              ?>
              <div class="card-footer">
                <div class="callout callout-secondary">
                  <h5><i class="fad fa-monitor-heart-rate"></i> INFO TITIK PANAS</h5>
                  <p><?=$datHotspot[COL_NM_DESC]?></p>

                  <?php
                  if(!empty($datHotspot[COL_NM_WARNING])) {
                    ?>
                    <p class="text-danger">
                      <strong>HIMBAUAN:</strong><br />
                      <?=$datHotspot[COL_NM_WARNING]?>
                    </p>
                    <?php
                  }
                  ?>
                </div>
              </div>
              <?php
            }
          }
          ?>
        </div>
      </div>
      <!--<div class="col-lg-6">
        <div class="card">
          <div class="card-header">
            <h5 class="card-title m-0">Prakiraan Cuaca Besok <small class="text-info"><?=date('d-m-Y', strtotime(date('Y-m-d').'+1 days'))?></small></h5>
          </div>
          <div class="card-body p-0">

          </div>
          <div class="card-footer">
          </div>
        </div>
      </div>-->
      <div class="col-lg-5">
        <div class="card">
          <div class="card-header">
            <h5 class="card-title m-0">Info Gempa Bumi Terkini</h5>
          </div>
          <div class="card-body p-0">
            <div id="bmkg-data-gempa">
              <p>Loading...</p>
            </div>
            <p class="p-2 font-italic text-left">
              <small>Sumber data: <br  /><strong>Badan Meteorologi, Klimatologi, dan Geofisika</strong></small>
            </p>
          </div>
          <div class="card-footer text-center">
            <a class="text-info" href="https://www.bmkg.go.id/gempabumi/gempabumi-terkini.bmkg" target="_blank">LIHAT SELENGKAPNYA</a>
          </div>
        </div>

        <div class="card">
          <div class="card-header">
            <h5 class="card-title m-0">Berita</h5>
          </div>
          <div class="card-body">
            <div class="row">
              <?php
              if(!empty($berita)) {
                ?>
                <div class="timeline timeline-inverse mb-0 w-100">
                  <?php
                  foreach($berita as $n) {
                    ?>
                    <div class="row m-0 mb-2">
                      <div class="timeline-item m-0 w-100">
                        <!--<span class="time"><i class="fad fa-calendar"></i> <?=date('d-m-Y', strtotime($n[COL_POSTDATE]))?></span>-->
                        <h3 class="timeline-header font-weight-bold">
                          <a href="<?=site_url('site/home/page/'.$n[COL_POSTSLUG])?>"><?=strlen($n[COL_POSTTITLE]) > 50 ? substr($n[COL_POSTTITLE], 0, 50) . "..." : $n[COL_POSTTITLE] ?></a>
                        </h3>
                        <div class="timeline-body pt-0">
                          <p class="m-0 mb-2">
                            <small class="text-muted">Oleh <strong><?=$n[COL_NAME]?></strong> pada <strong><?=date('d-m-Y', strtotime($n[COL_POSTDATE]))?></strong></small>
                          </p>
                          <?php
                          $strippedcontent = strip_tags($n[COL_POSTCONTENT]);
                          ?>
                          <?=strlen($strippedcontent) > 200 ? substr($strippedcontent, 0, 200) . "..." : $strippedcontent ?>
                        </div>
                      </div>
                    </div>
                    <?php
                  }
                   ?>
                </div>
                <?php
              } else {
                ?>
                <p class="font-italic">
                  Tidak ada data untuk ditampilkan.
                </p>
                <?php
              }
               ?>
            </div>
          </div>
          <div class="card-footer text-center">
            <a class="text-info" href="<?=site_url('site/home/post/1')?>">LIHAT SELENGKAPNYA</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
  $('[data-toggle="collapse"]', $('#accordionToday')).click(function() {
    var dis = $(this);
    if(!$(dis.attr('href')).hasClass('show')) {
      $('.panel-collapse.show', $('#accordionToday')).closest('.card').find('[data-toggle="collapse"]').click();
    }
  });
  $('[data-toggle="collapse"]', $('#accordionTomorrow')).click(function() {
    var dis = $(this);
    if(!$(dis.attr('href')).hasClass('show')) {
      $('.panel-collapse.show', $('#accordionTomorrow')).closest('.card').find('[data-toggle="collapse"]').click();
    }
  });

  $.get('https://data.bmkg.go.id/gempaterkini.xml', function(xml) {
    var gempa = $(xml).find('gempa');
    if(gempa.length == 0) {
      $('#bmkg-data-gempa').html('<p class="font-italic">Tidak ada data.</p>')
    } else {
      var html = '';
      html += '<ul class="products-list product-list-in-card pl-2 pr-2">';
      for(var i=0; i<gempa.length; i++) {
        if(i>=5) {
          break;
        }

        var badge = 'warning';
        if(parseFloat($(gempa[i]).find('Magnitude').text()) >= 7) {
          badge = 'danger';
        }
        html += '<li class="item">';
        html += '<div class="product-info ml-2">';
        html += '<p class="product-title m-0">'+$(gempa[i]).find('Tanggal').text()+' '+$(gempa[i]).find('Jam').text()+'<span class="badge badge-'+badge+' float-right">'+$(gempa[i]).find('Magnitude').text()+'</span></p>';
        html += '<span class="product-description">'+$(gempa[i]).find('Wilayah').text()+'<br  /><span class="font-italic">'+$(gempa[i]).find('Lintang').text()+', '+$(gempa[i]).find('Bujur').text()+'</span></span>';
        html += '</div>';
        html += '</li>';
      }
      html += '</ul>';
      $('#bmkg-data-gempa').html(html);
    }
  });
});
</script>
