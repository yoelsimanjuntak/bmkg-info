<section class="content-header">
  <div class="container">
    <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark"><?= $title ?>
        </div>
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fad fa-home"></i> Home</a></li>
                <li class="breadcrumb-item active"><?=$title?></li>
            </ol>
        </div>
    </div>
  </div>
</section>
<section class="content">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-outline card-info">
          <div class="card-header">
            <form id="form-search" action="#" method="post">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="fad fa-search"></i>&nbsp;&nbsp;Cari</span>
                </div>
                <input type="text" name="keyword" placeholder="Keyword.." class="form-control">
              </div>
            </form>
          </div>
          <div class="card-body p-0">
            <table id="tbl-res" class="table table-hover projects">
              <thead>
                <tr>
                  <th>Judul</th>
                  <th>Dilihat</th>
                </tr>
              </thead>
              <tbody>
                <?php
                foreach($res as $r) {
                  ?>
                  <tr>
                    <td>
                      <?=anchor('site/home/page/'.$r[COL_POSTSLUG],$r[COL_POSTTITLE])?><br  />
                      <small><?=date('d-m -Y',strtotime($r[COL_CREATEDON]))?> - <?=$r[COL_NAME]?></small>
                    </td>
                    <td class="text-right">
                      <?=number_format($r[COL_TOTALVIEW])?>
                    </td>
                  </tr>
                  <?php
                }
                 ?>
                 <tr class="tr-none" style="display: none">
                   <td colspan="2">Tidak menemukan tautan dengan kata kunci diatas.</td>
                 </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script>
$(document).ready(function() {
  $('[name=keyword]', $('#form-search')).on('keyup', function() {
    $('.tr-none', $('#tbl-res>tbody')).hide();

    var keyword = $(this).val();
    if(keyword) {
      $('tr', $('#tbl-res>tbody')).hide();
      var row = $("td", $('#tbl-res>tbody')).filter(function() {
        var reg = new RegExp(keyword, "i");
        return reg.test($(this).html());
      });
      if(row.length>0) {
        row.closest('tr').show();
      }
      else {
        $('.tr-none', $('#tbl-res>tbody')).show();
      }
    } else {
      $('tr', $('#tbl-res>tbody')).show();
    }

  });
});
</script>
