<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark"><?= $title ?> <small> Form</small></h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="breadcrumb-item"><a href="<?=site_url('site/data/info-index/'.$type)?>"> <?=$title?></a></li>
          <li class="breadcrumb-item active"><?=$edit?'Edit':'Add'?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <?=form_open_multipart(current_url(), array('role'=>'form','id'=>'main-form','class'=>'form-horizontal'))?>
    <div class="row">
      <div class="col-md-12 mb-2">
        <a href="<?=site_url('site/data/info-index/'.$type)?>" class="btn btn-secondary"><i class="fas fa-chevron-left"></i> KEMBALI</a>
        <button type="submit" class="btn btn-info"><i class="fad fa-save"></i> SIMPAN</button>
      </div>
      <div class="col-sm-12">
        <?php
        if ($this->input->get('error') == 1) {
            ?>
          <div class="callout callout-danger">
            <span class="text-danger"><i class="fa fa-ban"></i>&nbsp;&nbsp;Data gagal disimpan, silahkan coba kembali.</span>
          </div>
          <?php
        }
        if (validation_errors()) {
            ?>
          <div class="callout callout-danger">
            <span class="text-danger"><i class="fa fa-ban"></i>&nbsp;&nbsp;<?=validation_errors()?></span>
          </div>
          <?php
        }
        if (isset($err)) {
            ?>
          <div class="callout callout-danger">
            <span class="text-danger"><i class="fa fa-ban"></i>&nbsp;&nbsp;<?=$err['code'].' : '.$err['message']?></span>
          </div>
          <?php
        }
        if (!empty($upload_errors)) {
            ?>
          <div class="callout callout-danger">
            <span class="text-danger"><?=$upload_errors?></span>
          </div>
          <?php
        }
        ?>
      </div>
      <div class="clearfix"></div>
      <div class="col-sm-12">
        <div class="card card-outline card-default">
          <div class="card-body">
            <div class="form-group row">
                <label class="control-label col-sm-2">Tanggal</label>
                <div class="col-sm-2">
                    <input type="text" class="form-control datepicker" name="<?=COL_TANGGAL?>" />
                </div>
            </div>
            <div class="form-group row">
                <label class="control-label col-sm-2">Keterangan</label>
                <div class="col-sm-6">
                  <textarea class="form-control" name="<?=COL_NM_DESC?>" rows="3" placeholder="Keterangan"><?=$edit?$data[COL_NM_DESC]:''?></textarea>
                </div>
            </div>
            <div class="form-group row">
                <label class="control-label col-sm-2">Himbauan</label>
                <div class="col-sm-6">
                  <textarea class="form-control" name="<?=COL_NM_WARNING?>" rows="3" placeholder="Himbauan"><?=$edit?$data[COL_NM_WARNING]:''?></textarea>
                </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-sm-2">Gambar / Media</label>
              <div class="col-sm-4">
                <input type="file" name="files[]" accept="image/*,video/*" multiple="multiple" />
              </div>
              <?php
              if($edit && !empty($data[COL_IMAGES])) {
                $arrImg = json_decode($data[COL_IMAGES]);
                $arrLink = array();
                foreach($arrImg as $img) {
                  $arrLink[] = anchor(MY_UPLOADURL.$img, $img,array('target'=>'_blank'));
                }
                ?>
                <div class="col-sm-10 offset-sm-2">
                  <p><?=implode(' | ',$arrLink)?></p>
                </div>
                <?php
              }
              ?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?=form_close()?>
  </div>
</section>
