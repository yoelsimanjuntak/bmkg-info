<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark"><?= $title ?></h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <?=form_open(current_url(), array('role'=>'form', 'method'=>'get','id'=>'main-form','class'=>'form-horizontal'))?>
        <div class="card card-outline card-default">
          <div class="card-body">
            <div class="form-group row">
                <label class="control-label col-sm-3">Tanggal</label>
                <div class="col-sm-2">
                    <input type="text" class="form-control datepicker" name="<?=COL_TANGGAL?>" value="<?= !empty($data) ? $data[COL_TANGGAL] : ""?>" required />
                </div>
            </div>
          </div>
          <div class="card-footer text-right">
            <button type="submit" class="btn btn-info"><i class="fad fa-arrow-right"></i>&nbsp;GENERATE</button>
            <?php
            if(!empty($res)) {
              ?>
              <button type="submit" class="btn btn-secondary" name="CETAK" value="1"><i class="fad fa-print"></i>&nbsp;CETAK</button>
              <?php
            }
            ?>
          </div>
        </div>
        <?=form_close()?>
      </div>
      <div class="col-sm-12">
        <?php
        if(!empty($res) && !empty($data)) {
          ?>
          <div class="card card-outline card-default">
            <div class="card-header">
              <h5 class="card-title text-center float-none font-weight-light">INFO PELAYANAN BMKG-SILANGIT <?=date('d-m-Y', strtotime($data[COL_TANGGAL]))?></h5>
            </div>
            <div class="card-body">
              <?=$this->load->view('data/report_partial');?>
            </div>
          </div>
          <?php
        }
        ?>
      </div>
    </div>
  </div>
</section>
