<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark"><?= $title ?> <small class="font-weight-light">Data</small></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="breadcrumb-item active"><?=$title?></li>
                </ol>
            </div>
        </div>
    </div>
</div>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <p>
                  <?=anchor('site/data/info-delete', '<i class="fas fa-trash"></i> Hapus', array('class'=>'cekboxaction btn btn-danger btn-sm','data-confirm'=>'Apa anda yakin?'))?>
                  <?=anchor('site/data/info-add/'.$type, '<i class="fas fa-plus"></i> Data Baru', array('class'=>'btn btn-primary btn-sm'))?>
                  <?php
                  if(GetLoggedUser()[COL_ROLEID] != ROLESTAFF) {
                    ?>
                    <?=anchor('site/data/info-publish', '<i class="fas fa-check"></i> Publish', array('class'=>'cekboxaction btn btn-success btn-sm','data-confirm'=>'Tayangkan data ini?'))?>
                    <?=anchor('site/data/info-publish/-1', '<i class="fas fa-times"></i> Unpublish', array('class'=>'cekboxaction btn btn-warning btn-sm','data-confirm'=>'Berhenti tayangkan data ini?'))?>
                    <?php
                  }
                   ?>
                </p>
                <div class="card card-default">
                    <div class="card-body">
                        <form id="dataform" method="post" action="#">
                            <table id="datalist" class="table table-bordered table-hover">

                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function() {
        var dataTable = $('#datalist').dataTable({
          "autoWidth" : false,
          "processing": true,
          "serverSide": true,
          "ajax": {
            url : "<?=site_url('site/data/info-index-load/'.$type)?>",
            type : 'POST'
          },
          //"sDom": "Rlfrtip",
          //"aaData": <?=$data?>,
          //"bJQueryUI": true,
          //"aaSorting" : [[5,'desc']],
          "scrollY" : '40vh',
          "scrollX": "200%",
          "iDisplayLength": 100,
          "aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
          "dom":"R<'row'<'col-sm-4'l><'col-sm-4'B><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
          "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
          "order": [[ 1, "desc" ]],
          "columnDefs": [{"targets":[4], "className":'nowrap text-center'}, {"targets":[0,1,5,6], "className":'nowrap'}],
          "aoColumns": [
              {"sTitle": "<input type=\"checkbox\" id=\"cekbox\" class=\"\" />","width":"10px","bSortable":false},
              {"sTitle": "Tanggal"},
              //{"sTitle": "Detail","bSortable":false},
              {"sTitle": "Keterangan","bSortable":false},
              {"sTitle": "Himbauan","bSortable":false},
              {"sTitle": "Publish","bSortable":false},
              {"sTitle": "Dibuat oleh","bSortable":false},
              {"sTitle": "Dibuat pada"},
          ],
          "drawCallback": function(a) {

          }
        });
        $('#cekbox').click(function(){
            if($(this).is(':checked')){
                $('.cekbox').prop('checked',true);
            }else{
                $('.cekbox').prop('checked',false);
            }
        });
    });
</script>
