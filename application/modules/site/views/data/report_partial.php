<?php
$datEarthquake = $this->db
->where(array(COL_NM_TYPE=>'EARTHQUAKE', COL_TANGGAL=>(!empty($data[COL_TANGGAL])?$data[COL_TANGGAL]:date('Y-m-d'))))
->get(TBL_BMKG_TEARTHQUAKE)
->row_array();
$datTsunami = $this->db
->where(array(COL_NM_TYPE=>'TSUNAMI', COL_TANGGAL=>(!empty($data[COL_TANGGAL])?$data[COL_TANGGAL]:date('Y-m-d'))))
->get(TBL_BMKG_TEARTHQUAKE)
->row_array();
$datHotspot = $this->db
->where(array(COL_NM_TYPE=>'HOTSPOT', COL_TANGGAL=>(!empty($data[COL_TANGGAL])?$data[COL_TANGGAL]:date('Y-m-d'))))
->get(TBL_BMKG_TEARTHQUAKE)
->row_array();
?>
<style>
.vertical{
  writing-mode: vertical-lr;
  transform: rotate(180deg);
}
.table {
  font-family: Arial,sans-serif;
  border-collapse: collapse;
}
.table td, .table th {
  padding: .75rem;
  vertical-align: top;
  text-align: center;
}
.table-bordered td, .table-bordered th {
  border: 1px solid #dee2e6;
}
.bg-info {
  background-color: #17a2b8!important;
  color: #fff!important;
}
.bg-warning {
  background-color: #ffc107!important;
  color: #1f2d3d!important;
}
.bg-danger {
  background-color: #dc3545!important;
  color: #fff!important;
}
.bg-gray {
  background-color: #6c757d!important;;
  color: #fff!important;
}
.h5, h5 {
  font-size: 1.25rem;
}
.h1, .h2, .h3, .h4, .h5, .h6, h1, h2, h3, h4, h5, h6 {
  margin-bottom: .5rem;
  font-family: inherit;
  font-weight: 500;
  line-height: 1.2;
  color: inherit;
}
p {
  font-size: 1rem;
}
.text-danger {
    color: #dc3545!important;
}
</style>
<?php
if($page == 'all' || $page == '1') {
  ?>
  <table class="table table-bordered">
    <thead>
      <tr>
        <td style="width: 50%; font-size: 2rem">PRAKIRAAN CUACA HARI INI</td>
        <td style="width: 50%; font-size: 2rem">PRAKIRAAN CUACA ESOK HARI</td>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td colspan="2" style="text-align: center; font-weight: bold" class="bg-info">PAGI</td>
      </tr>
      <tr>
        <td>
          <h5 class="text-center">
            <?=$res['Nm_FC1']?>
          </h5>
          <p class="m-0 text-center">
            <?=$res[COL_FC1_DESC]?>
          </p>
        </td>
        <td>
          <h5 class="text-center">
            <?=$res['Nm_FCT1']?>
          </h5>
          <p class="m-0 text-center">
            <?=$res[COL_FCT1_DESC]?>
          </p>
        </td>
      </tr>
      <tr>
        <td colspan="2" style="text-align: center; font-weight: bold" class="bg-warning">SIANG</td>
      </tr>
      <tr>
        <td>
          <h5 class="text-center">
            <?=$res['Nm_FC2']?>
          </h5>
          <p class="m-0 text-center">
            <?=$res[COL_FC2_DESC]?>
          </p>
        </td>
        <td>
          <h5 class="text-center">
            <?=$res['Nm_FCT2']?>
          </h5>
          <p class="m-0 text-center">
            <?=$res[COL_FCT2_DESC]?>
          </p>
        </td>
      </tr>
      <tr>
        <td colspan="2" style="text-align: center; font-weight: bold" class="bg-danger">SORE</td>
      </tr>
      <tr>
        <td>
          <h5 class="text-center">
            <?=$res['Nm_FC3']?>
          </h5>
          <p class="m-0 text-center">
            <?=$res[COL_FC3_DESC]?>
          </p>
        </td>
        <td>
          <h5 class="text-center">
            <?=$res['Nm_FCT3']?>
          </h5>
          <p class="m-0 text-center">
            <?=$res[COL_FCT3_DESC]?>
          </p>
        </td>
      </tr>
      <tr>
        <td colspan="2" style="text-align: center; font-weight: bold" class="bg-gray">MALAM</td>
      </tr>
      <tr>
        <td>
          <h5 class="text-center">
            <?=$res['Nm_FC4']?>
          </h5>
          <p class="m-0 text-center">
            <?=$res[COL_FC4_DESC]?>
          </p>
        </td>
        <td>
          <h5 class="text-center">
            <?=$res['Nm_FCT4']?>
          </h5>
          <p class="m-0 text-center">
            <?=$res[COL_FCT4_DESC]?>
          </p>
        </td>
      </tr>
      <tr>
        <td colspan="2" style="text-align: center; font-weight: bold">PERINGATAN</td>
      </tr>
      <tr>
        <td>
          <p><?=!empty($res[COL_ALERT_WARNING]) ? $res[COL_ALERT_WARNING] : '-'?></p>
        </td>
        <td>
          <p><?=!empty($res[COL_ALERT_TWARNING]) ? $res[COL_ALERT_TWARNING] : '-'?></p>
        </td>
      </tr>
      <tr>
        <td colspan="2" style="text-align: center; font-weight: bold">HIMBAUAN</td>
      </tr>
      <tr>
        <td>
          <p><?=!empty($res[COL_ALERT_TEXT]) ? $res[COL_ALERT_TEXT] : '-'?></p>
        </td>
        <td>
          <p><?=!empty($res[COL_ALERT_TTEXT]) ? $res[COL_ALERT_TTEXT] : '-'?></p>
        </td>
      </tr>
    </tbody>
  </table>
  <?php
}
?>
<br />
<?php
if($page == 'all' || $page == '2') {
  ?>
  <table class="table table-bordered">
    <thead>
      <tr>
        <td style="width: 33.3333%; font-size: 2rem">INFO GEMPA</td>
        <td style="width: 33.3333%; font-size: 2rem">INFO TSUNAMI</td>
        <td style="width: 33.3333%; font-size: 2rem">INFO TITIK PANAS</td>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>
          <?php
          if(!empty($datEarthquake)) {
            $imgs = json_decode($datEarthquake[COL_IMAGES]);
            if(!empty($imgs) && count($imgs) > 0) {
              foreach($imgs as $img) {
                if(!file_exists(MY_UPLOADPATH.$img)) continue;
                if(strpos(mime_content_type(MY_UPLOADPATH.$img), 'video') !== false) {
                  ?>
                  <video width="100%" controls>
                    <source src="<?=MY_UPLOADURL.$img?>" type="<?=mime_content_type(MY_UPLOADPATH.$img)?>"  />
                    Your browser does not support the video tag.
                  </video>
                  <?php
                } else if(strpos(mime_content_type(MY_UPLOADPATH.$img), 'image') !== false) {
                  ?>
                  <img src="<?=MY_UPLOADURL.$img?>" width="200vh" />
                  <?php
                } else if(strpos(mime_content_type(MY_UPLOADPATH.$img), 'application/pdf') !== false) {
                  ?>
                  <p><?=anchor(MY_UPLOADURL.$img, '<i class="fad '.(!empty($cetak)?'d-none':'').' fa-paperclip"></i>&nbsp;'.$img, array('target' => '_blank'))?></p>
                  <?php
                }
              }
            }
            ?>
            <p><?=$datEarthquake[COL_NM_DESC]?></p>
            <?php
            if(!empty($datEarthquake[COL_NM_WARNING])) {
              ?>
              <br />
              <p class="text-danger">
                <strong>HIMBAUAN:</strong><br />
                <?=$datEarthquake[COL_NM_WARNING]?>
              </p>
              <?php
            }
            ?>
            <?php
          } else {
            echo '-';
          }
          ?>
        </td>
        <td>
          <?php
          if(!empty($datTsunami)) {
            $imgs = json_decode($datTsunami[COL_IMAGES]);
            if(!empty($imgs) && count($imgs) > 0) {
              foreach($imgs as $img) {
                if(!file_exists(MY_UPLOADPATH.$img)) continue;
                if(strpos(mime_content_type(MY_UPLOADPATH.$img), 'video') !== false) {
                  ?>
                  <video width="100%" controls>
                    <source src="<?=MY_UPLOADURL.$img?>" type="<?=mime_content_type(MY_UPLOADPATH.$img)?>"  />
                    Your browser does not support the video tag.
                  </video>
                  <?php
                } else if(strpos(mime_content_type(MY_UPLOADPATH.$img), 'image') !== false) {
                  ?>
                  <img src="<?=MY_UPLOADURL.$img?>" width="200vh" />
                  <?php
                } else if(strpos(mime_content_type(MY_UPLOADPATH.$img), 'application/pdf') !== false) {
                  ?>
                  <p><?=anchor(MY_UPLOADURL.$img, '<i class="fad '.(!empty($cetak)?'d-none':'').' fa-paperclip"></i>&nbsp;'.$img, array('target' => '_blank'))?></p>
                  <?php
                }
              }
            }
            ?>
            <p><?=$datTsunami[COL_NM_DESC]?></p>
            <?php
            if(!empty($datTsunami[COL_NM_WARNING])) {
              ?>
              <br />
              <p class="text-danger">
                <strong>HIMBAUAN:</strong><br />
                <?=$datTsunami[COL_NM_WARNING]?>
              </p>
              <?php
            }
            ?>
            <?php
          } else {
            echo '-';
          }
          ?>
        </td>
        <td>
          <?php
          if(!empty($datHotspot)) {
            $imgs = json_decode($datHotspot[COL_IMAGES]);
            if(!empty($imgs) && count($imgs) > 0) {
              foreach($imgs as $img) {
                if(!file_exists(MY_UPLOADPATH.$img)) continue;
                if(strpos(mime_content_type(MY_UPLOADPATH.$img), 'video') !== false) {
                  ?>
                  <video width="100%" controls>
                    <source src="<?=MY_UPLOADURL.$img?>" type="<?=mime_content_type(MY_UPLOADPATH.$img)?>"  />
                    Your browser does not support the video tag.
                  </video>
                  <?php
                } else if(strpos(mime_content_type(MY_UPLOADPATH.$img), 'image') !== false) {
                  ?>
                  <img src="<?=MY_UPLOADURL.$img?>" width="200vh" />
                  <?php
                } else if(strpos(mime_content_type(MY_UPLOADPATH.$img), 'application/pdf') !== false) {
                  ?>
                  <p><?=anchor(MY_UPLOADURL.$img, '<i class="fad '.(!empty($cetak)?'d-none':'').' fa-paperclip"></i>&nbsp;'.$img, array('target' => '_blank'))?></p>
                  <?php
                }
              }
            }
            ?>
            <p><?=$datHotspot[COL_NM_DESC]?></p>
            <?php
            if(!empty($datHotspot[COL_NM_WARNING])) {
              ?>
              <br />
              <p class="text-danger">
                <strong>HIMBAUAN:</strong><br />
                <?=$datHotspot[COL_NM_WARNING]?>
              </p>
              <?php
            }
            ?>
            <?php
          } else {
            echo '-';
          }
          ?>
        </td>
      </tr>
    </tbody>
  </table>
  <?php
}
?>
