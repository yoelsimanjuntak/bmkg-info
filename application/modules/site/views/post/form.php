<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark"><?= $title ?> <small class="font-weight-light"> Form</small></h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="breadcrumb-item"><a href="<?=site_url('site/post/index')?>"> <?=$title?></a></li>
          <li class="breadcrumb-item active"><?=$edit?'Edit':'Add'?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <?=form_open_multipart(current_url(), array('role'=>'form','id'=>'main-form','class'=>'form-horizontal'))?>
    <div class="row">
      <div class="col-sm-12">
        <?php
        if ($this->input->get('error') == 1) {
            ?>
          <div class="callout callout-danger">
            <span class="text-danger"><i class="fa fa-ban"></i>&nbsp;&nbsp;Data gagal disimpan, silahkan coba kembali.</span>
          </div>
          <?php
        }
        if (validation_errors()) {
            ?>
          <div class="callout callout-danger">
            <span class="text-danger"><i class="fa fa-ban"></i>&nbsp;&nbsp;<?=validation_errors()?></span>
          </div>
          <?php
        }
        if (isset($err)) {
            ?>
          <div class="callout callout-danger">
            <span class="text-danger"><i class="fa fa-ban"></i>&nbsp;&nbsp;<?=$err['code'].' : '.$err['message']?></span>
          </div>
          <?php
        }
        if (!empty($upload_errors)) {
            ?>
          <div class="callout callout-danger">
            <span class="text-danger"><?=$upload_errors?></span>
          </div>
          <?php
        }
        ?>
      </div>
      <div class="col-sm-12">
        <div class="card card-outline card-default">
          <div class="card-body">
            <div class="form-group row">
              <label class="control-label col-sm-2">Judul</label>
              <div class="col-sm-10">
                <div class="input-group">
                    <input type="text" class="form-control" name="<?=COL_POSTTITLE?>" value="<?=!empty($data[COL_POSTTITLE]) ? $data[COL_POSTTITLE] : ''?>" placeholder="Title" required />
                    <div class="input-group-append">
                      <span class="input-group-text">
                          <i class="fad fa-text"></i>
                      </span>
                    </div>
                </div>
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-sm-2">Kategori</label>
              <div class="col-sm-10">
                <div class="input-group">
                    <select name="<?=COL_POSTCATEGORYID?>" class="form-control" required>
                        <?=GetCombobox("SELECT * FROM _postcategories ORDER BY PostCategoryName", COL_POSTCATEGORYID, COL_POSTCATEGORYNAME, (!empty($data[COL_POSTCATEGORYID]) ? $data[COL_POSTCATEGORYID] : null))?>
                    </select>
                    <div class="input-group-append">
                      <span class="input-group-text">
                          <i class="fad fa-list"></i>
                      </span>
                    </div>
                </div>
              </div>

            </div>
            <div class="form-group row">
              <label class="control-label col-sm-2">Tayang s.d</label>
              <div class="col-sm-10">
                <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                          <i class="fad fa-calendar"></i>
                      </span>
                    </div>
                    <input type="text" class="form-control datepicker" name="<?=COL_POSTEXPIREDDATE?>" value="<?=!empty($data[COL_POSTEXPIREDDATE]) ? date('Y-m-d', strtotime($data[COL_POSTEXPIREDDATE])) : ''?>" placeholder="Expired Date" required>
                    <div class="input-group-append">
                      <span class="input-group-text">
                          <label class="mb-0"><input type="checkbox" name="<?=COL_ISSUSPEND?>" <?=!empty($data[COL_ISSUSPEND]) && $data[COL_ISSUSPEND]?"checked":""?> value="1" />&nbsp;&nbsp;Suspend</label>
                      </span>
                    </div>
                </div>
              </div>
            </div>
            <div class="form-group row">
                <label class="control-label col-sm-2">Lampiran (Image / PDF)</label>
                <div class="col-sm-10">
                  <input type="file" name="userfile[]" accept="image/*, application/pdf" multiple />
                  <p class="help-block font-italic text-gray">Optional, Mutiple, maks. 50MB</p>
                </div>
            </div>
            <?php
            if(!empty($data[COL_POSTID])) {
                $files_ = $this->db->where(COL_POSTID, $data[COL_POSTID])->get(TBL__POSTIMAGES)->result_array();
                ?>
                <div class="row">
                  <div class="col-md-10 offset-md-2">
                      <?php
                      foreach($files_ as $f) {
                        if(!file_exists(MY_UPLOADPATH.$f[COL_FILENAME])) continue;
                        if(strpos(mime_content_type(MY_UPLOADPATH.$f[COL_FILENAME]), 'image') !== false) {
                          ?>
                          <img class="m-2" src="<?=MY_UPLOADURL.$f[COL_FILENAME]?>" alt="<?=$f[COL_DESCRIPTION]?>" style="height: 20vh; border: 1.5px solid #dedede; box-shadow: 3px 4px 5px rgba(0,0,0,0.2);" />
                          <?php
                        } else if(strpos(mime_content_type(MY_UPLOADPATH.$f[COL_FILENAME]), 'application/pdf') !== false) {
                          ?>
                          <p><?=anchor(MY_UPLOADURL.$f[COL_FILENAME], '<i class="fad fa-paperclip"></i>&nbsp;'.$f[COL_FILENAME], array('target' => '_blank'))?></p>
                          <?php
                        }
                        ?>
                      <?php
                      }
                      ?>
                  </div>
                </div>
            <?php } ?>
            <div class="form-group">
                <label>Content</label>
                <textarea id="content_editor" class="form-control" rows="4" placeholder="Post Content" name="<?=COL_POSTCONTENT?>"><?=!empty($data[COL_POSTCONTENT]) ? $data[COL_POSTCONTENT] : ''?></textarea>
            </div>
          </div>
          <div class="card-footer text-center">
            <a href="<?=site_url('site/post/index')?>" class="btn btn-default"><i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Kembali ke Daftar</a>&nbsp;
            <button type="submit" class="btn btn-primary"><i class="fad fa-save"></i> Simpan</button>
          </div>
        </div>
      </div>
    </div>
    <?=form_close()?>
  </div>
</section>
<script>
    $(document).ready(function() {
        $("[name=PostTitle]").change(function() {
            var title = $(this).val().toLowerCase();
            var slug = title.replace(/\s/g, "-");
            $("[name=PostSlug]").val(slug);
        }).trigger("change");

        CKEDITOR.config.height = 400;
        CKEDITOR.replace("content_editor");
    });
</script>
