# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.36-MariaDB)
# Database: hh_bmkg
# Generation Time: 2020-04-27 09:19:23 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table bmkg_tearthquake
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bmkg_tearthquake`;

CREATE TABLE `bmkg_tearthquake` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `Tanggal` date NOT NULL,
  `NM_Type` varchar(50) DEFAULT NULL,
  `Num_Magnitude` double DEFAULT NULL,
  `Num_Depth` double DEFAULT NULL,
  `Num_Lat` double DEFAULT NULL,
  `Num_Long` double DEFAULT NULL,
  `Nm_Desc` varchar(200) NOT NULL DEFAULT '',
  `Nm_Warning` varchar(200) DEFAULT NULL,
  `Nm_WarningFrom` varchar(5) DEFAULT NULL,
  `Nm_WarningTo` varchar(5) DEFAULT NULL,
  `Images` text,
  `Published` tinyint(1) DEFAULT NULL,
  `Published_By` varchar(50) DEFAULT NULL,
  `Published_Date` datetime DEFAULT NULL,
  `Create_By` varchar(50) NOT NULL DEFAULT '',
  `Create_Date` datetime NOT NULL,
  `Edit_By` varchar(50) DEFAULT NULL,
  `Edit_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `bmkg_tearthquake` WRITE;
/*!40000 ALTER TABLE `bmkg_tearthquake` DISABLE KEYS */;

INSERT INTO `bmkg_tearthquake` (`Uniq`, `Tanggal`, `NM_Type`, `Num_Magnitude`, `Num_Depth`, `Num_Lat`, `Num_Long`, `Nm_Desc`, `Nm_Warning`, `Nm_WarningFrom`, `Nm_WarningTo`, `Images`, `Published`, `Published_By`, `Published_Date`, `Create_By`, `Create_Date`, `Edit_By`, `Edit_Date`)
VALUES
	(2,'2020-04-27','EARTHQUAKE',NULL,NULL,NULL,NULL,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat..',NULL,NULL,'[\"gn-001-gundam-exia-gn001-gundam-exia-png-clip-art2.png\"]',1,NULL,NULL,'admin','2020-04-27 15:55:50','admin','2020-04-27 16:04:48');

/*!40000 ALTER TABLE `bmkg_tearthquake` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
